from reactor.utils import *

#*******************************************************************************

def depack(**items):
    return [
        '%s.%s' % (ns,path)
        for ns,lst in items.iteritems() for path in lst
    ]

#*******************************************************************************

MAIN_DOMAIN = 'apis.uchikoma.faith'

GRID_DNS = None
#GRID_DNS = 'it-issal.top'

################################################################################

APPLETs = {
    'connector': dict(
        components=['Bookmarks']+depack(
            Social  = ['People'],
            #Talker  = ['HipChat','Slack'],
            Telecom = ['MailBox'],
        ),
        urlconf={
            'apis': 'APIs',
            'auth': 'Auth',

            'connect': 'Connect',
        },
    ),
    'console': dict(
        components=depack(
            Cables   = ['Feeds','News'],
            Crawling = ['Spiders'],
            #Search   = ['Google'],
        ),
        urlconf={
            'hub':  'Hub',

            #'coop':  'Cooperate',
            #'tasks': 'Achieve',
        },
    ),
    'linked': dict(
        components=depack(
            #Semantic=['RDF','OwL','SparQL'],
        ),
        urlconf={
            #'schema':  'Schema',
            #'data':    'Data',
        },
    ),
    #---------------------------------------------------------------------------
    'infosec': dict(
        components=depack(
            #Protocols=['IPv4','IPv6'],
            Discovery=[
                'DNS','NetBIOS','ZeroConf',
            ],
            Offensive=['Scanner'],
        ),
        urlconf={
        },
    ),
    'linguist': dict(
        components=depack(
            Analyzer = ['Blob_Processor'],
            Parser   = ['Wernicke','Broca'],
            #Talker   = ['Answer.Que','Learner.Markov'],
        ),
        urlconf={
            #'lang':  'nucleon.linguist.urls',
        },
    ),
    #---------------------------------------------------------------------------
    'devops': dict(
        components=depack(
            Process  = ['MultiThreading'],
            Parallel = ['JobLib'],
            Virtual  = ['Contain','Emulate'],
        ),
        urlconf={
            'dev':  'nucleon.devops.views.Devel',
            'ops':  'nucleon.devops.views.SysOp',
            'iot':  'nucleon.devops.views.Fleet',
        },
    ),
    'explor': dict(
        components=depack(
            Local = ['POSIX'], #,'IPFS'],
            Cloud = ['MegaNZ'],
            #Grid  = ['IPFS'],
        ),
        urlconf={
            'vfs':  'nucleon.explor.views.FileSystem',
        },
    ),
    'hobbit': dict(
        components=depack(
            Serve = ['LiveStream'],
            Infos = ['TVDB','Trakt'],
        ),
        urlconf={
            #'www':  'nucleon.hobbit.urls',
        },
    ),
}

#*******************************************************************************

WAMP_MIDDLEWARES = [{
    "type": "class", "realm": "reactor", "role": "backend",
    "classname": "reactor.webapp.common.streams.Gateway.Native"
},{
    "type": "class", "realm": "reactor", "role": "backend",
    "classname": "reactor.webapp.common.streams.Gateway.Authority"
}]

#*******************************************************************************

WAMP_ENDPOINTS = [{
    "method":  "opengraph",
    "baseurl": "https://graph.facebook.com/",
    "realm":   "reactor",
}]

################################################################################

WAMP_WORKERS = [{
    "type": "guest",
    "executable": "supervisord",
    "arguments": ["-c", "kernel/bootstrap.conf", "-n"],
    "options": {
        "workdir": "../",
        "env": {
            "inherit": True,
            "vars": {
                "PERSON_ID": "tayamino",
                "LANGUAGE":  "en"
            }
        },
        "watch": {
            "directories": [
                "../kernel/"
            ],
            "action": "restart"
        }
    }
}]

#*******************************************************************************

WAMP_PROTOCOLS = {
    #'dns': {
    #    'PORT': 60100,
    #},
    #'http': {
    #    'PORT': 60100,
    #},
    #'smtp': {
    #    'PORT': 60100,
    #},
}

#*******************************************************************************

WAMP_BACKENDS = []

#*******************************************************************************

#WAMP_INSTANCES = [{
#    "program":   "npm",
#    "arguments": ["start"],
#    "workdir":   "../programs/scality",
#    "environ":   {
#        "PORT":  "60030"
#    },
#    "watch":     ['../nucleon'],
#}]

################################################################################

WAMP_PATHS = {
      "/": {
         "type": "wsgi",
         "module": "reactor.webapp.wsgi",
         "object": "application"
      },
      "media": {
         "type": "static",
         "directory": "../files/media",
         "options": {
            "enable_directory_listing": True,
            "mime_types": {
               ".svg": "image/svg+xml"
            }
         }
      },
      "static": {
         "type": "static",
         "directory": "../files/assets",
         "options": {
            "enable_directory_listing": True,
            "mime_types": {
               ".svg": "image/svg+xml"
            }
         }
      },
      "upload": {
        "type": "upload",
        "realm": "reactor",
        "role": "nerves",
        "directory": "../files/uploads/done",
        "temp_directory": "../files/uploads/temp",
        "form_fields": {
            "file_name": "resumableFilename",
            "mime_type": "resumableType",
            "total_size": "resumableTotalSize",
            "chunk_number": "resumableChunkNumber",
            "chunk_size": "resumableChunkSize",
            "total_chunks": "resumableTotalChunks",
            "content": "file",
            "on_progress": "on_progress",
            "session": "session",
            "chunk_extra": "chunk_extra",
            "finish_extra": "finish_extra"
        },
        "options": {
            "max_file_size": 200000000,
            "file_permissions": "0644",
            "file_types": [
                ".csv",
                ".txt",
                ".pdf",
                ".img",
                ".png",
                ".jpg",
                ".bz2",
                ".xz",
                ".gz"
            ]
        }
      },
      "longpoll" : {
         "type" : "longpoll"
      },
      "caller" : {
         "type": "caller",
         "realm": "reactor",
         "role": "backend",
         "options": {
             "debug": True
         }
      },
      "publisher" : {
         "type": "publisher",
         "realm": "reactor",
         "role": "backend",
         "options": {
             "debug": True
         }
      },
      "sockets" : {
         "type" : "websocket",
         "serializers": [ "json", "msgpack", "ubjson", "cbor" ],
         "auth": {
            "ticket": {
                "type": "dynamic",
                "authenticator": "reactor.authentify"
            }
         }
      },
      "hook-github" : {
         "type" : "webhook", "realm" : "reactor", "role" : "nerves",
         "options": { "topic": "reactor.apis.github.webhook" }
      },
      "hook-bitbucket" : {
         "type" : "webhook", "realm" : "reactor", "role" : "nerves",
         "options": { "topic": "reactor.apis.bitbucket.webhook" }
      }
}

################################################################################

PRINCIPALS_DB = {
   u"joe": {
        u"ticket": u"secret!!!",
        u"role": u"frontend"
   },
   u"webui": {
        u"ticket": u"123sekret",
        u"role": u"frontend"
   },
   u"astral": {
        u"ticket": u"projection",
        u"role": u"implant"
   },
   u"vernam": {
        u"ticket": u"multiverse",
        u"role": u"device"
   },
   u"raspberry": {
        u"ticket": u"debian",
        u"role": u"device"
   },
}

################################################################################

MYTICKET = os.environ.get('MYTICKET', 'testing')

if MYTICKET and six.PY2:
   MYTICKET = MYTICKET.decode('utf8')

#*******************************************************************************

for app,cfg in APPLETs.iteritems():
    for key in cfg.get('components', []):
        WAMP_MIDDLEWARES.append({
            "type": "class", "realm": "reactor", "role": "backend",
            "classname": "nucleon.%s.streams.%s" % (app,key)
        })

#*******************************************************************************

for ep in WAMP_ENDPOINTS:
    WAMP_MIDDLEWARES += [{
        "type": "class", "realm": ep['realm'], "role": "backend",
        "extra": {
            "procedure": "reactor.www.%(method)s" % ep,
            "baseurl":   ep['baseurl'],
        }, "classname": "crossbar.adapter.rest.RESTCallee"
    }]

#*******************************************************************************

for key in PRINCIPALS_DB.keys():
    PRINCIPALS_DB[key][u"role"] = u"backend"

