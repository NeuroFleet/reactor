from .libs import *

################################################################################

bpath = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
rpath = lambda *x: os.path.abspath(os.path.join(bpath, 'files', *x))

