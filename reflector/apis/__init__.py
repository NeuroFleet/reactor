from reactor.reflector.shortcuts import *

@Prism.wsgi.route("/api/")
def api_homepage():
    return render_template('special/console.html', widgets=[
    ])

from . import network
from . import vfs
from . import media
from . import linked

