from reactor.reflector.core.abstract import *

from redis import Redis

from flask_admin                     import Admin     as FlaskAdmin
from flask_admin.contrib.fileadmin   import FileAdmin
from flask_admin.contrib.rediscli    import RedisCli  as RedisAdmin

from flask_admin.contrib.sqla        import ModelView
from flask_admin.contrib.mongoengine import ModelView as DocumentView

##########################################################################

@Prism.register('web')
class WebManager(Prism.Manager):
    def initialize(self):
        self._adm = FlaskAdmin(Prism.wsgi, name='Psycho', template_mode='bootstrap3') #, endpoint='/manage/')

        #*******************************************************************************

        #admin.add_view(ModelView(User, db.session))
        #admin.add_view(ModelView(Post, db.session))

        #*******************************************************************************

        self._adm.add_view(FileAdmin(rpath('serve','media'),  '/media/', name='Multimedia'))
        #admin.add_view(FileAdmin(rpath('serve','assets'), '/files/', name='Static Files'))

        #*******************************************************************************

        self._adm.add_view(RedisAdmin(Redis(), 'Cache'))

        #*******************************************************************************

        from flask_admin.form import rules

        #*******************************************************************************

        #from social_flask_mongoengine.models import init_social

        #init_social(Prism.wsgi, session)

    ######################################################################

    def register_api(self, *args, **kwargs):
        def do_apply(handler, alias):
            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #*********************************************************************

    def register_view(self, *args, **kwargs):
        def do_apply(handler, alias):
            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

##########################################################################

@Prism.register('ui')
class UserInterface(Prism.Manager):
    def initialize(self):
        self._cnx = None

    #*********************************************************************

    def render(self, template, **context):
        target = Prism.wsgi.request_context(context)

        return render_template()
    
    ######################################################################

    def widget(self, *args, **kwargs):
        return self.BaseWidget(self, *args, **kwargs)

    #*********************************************************************

    def widgets(self, column, *listing):
        self._wdg = []

    ######################################################################

    class BaseForm(Renderable):
        pass

    ######################################################################

    class BaseWidget(Renderable):
        def initialize(self):
            pass

        template = property(lambda self: 'inc/blocks/%s.html' % self.narrow)

        @property
        def context(self):
            return dict([(k,v) for k,v in self._kws.iteritems()])

    ######################################################################

    class BaseType(Renderable):
        pass

