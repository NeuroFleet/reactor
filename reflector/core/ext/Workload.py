from reactor.reflector.core.abstract import *

##########################################################################

@Prism.register('task')
class TaskWorker(Prism.Manager):
    def initialize(self):
        self._cnx = None # RQ

    queues = property(lambda self: [])

    #*********************************************************************

    def enqueue(self, target_queue, target_method):
        def do_apply(target, method, args, kwargs):
            

            return None

        return lambda *x_args, **x_kwargs: do_apply(target_queue, target_method, x_args, x_kwargs)

    #*********************************************************************

    def dequeue(self, target_queue):
        pass

    #*********************************************************************

    def count(self, target_queue):
        pass

    #*********************************************************************

    def clear(self, target_queue):
        pass

##########################################################################

@Prism.register('func')
class Functionnal(Prism.Manager):
    def initialize(self):
        self._cnx = None

    languages = property(lambda self: [])

    #*********************************************************************

    def parse(self, src, lang=None):
        pass

