from reactor.reflector.libs import *

from os.path import abspath,dirname,join as pathjoin

#*******************************************************************************

bpath = dirname(dirname(dirname(dirname(dirname(dirname(__file__))))))
rpath = lambda *x: pathjoin(bpath, *x)

#*******************************************************************************

def singleton(handler):
    return handler()

