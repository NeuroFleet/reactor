from .helpers import *

##########################################################################

class Component(object):
    def __init__(self, *args, **kwargs):
        self.trigger('initialize', *args, **kwargs)

    def trigger(self, method_name, *args, **kwargs):
        hnd = getattr(self, method_name, None)

        if callable(hnd):
            return hnd(*args, **kwargs)
        else:
            return None

#*************************************************************************

class Manageable(Component):
    def __init__(self, mgr, *args, **kwargs):
        self._mgr = mgr

        super(Manageable, self).__init__(*args, **kwargs)

#*************************************************************************

class Renderable(Manageable):
    def __init__(self, mgr, narrow, title, *params, **attrs):
        self._nrw = narrow
        self._lbl = title

        self._hdn = None
        self._prm = params
        self._kws = attrs

        super(Renderable, self).__init__(mgr)

    narrow   = property(lambda self: self._nrw)
    title    = property(lambda self: self._lbl)
    heading  = property(lambda self: self._hdn or '')

    params   = property(lambda self: self._prm)
    attrs    = property(lambda self: self._kws)

    def __getitem__(self, key, default=None): return self._kws.get(key, default)

    def render(self):
        tpl,cnt = None, self.context

        try:
            tpl = Prism.wsgi.jinja_env.get_template(self.template)
        except TemplateNotFound,ex:
            return ""
        else:
            cnt['wdg']    = self
            cnt['widget'] = self

            cnt['data']   = self

            return tpl.render(cnt)

##########################################################################

@singleton
class Prism(object):
    def __init__(self):
        self._reg = {}

        self._mem = redis.StrictRedis(host='localhost', port=6379, db=0)

        self._app = Flask(__name__)
        self._app.debug = True

        self.wsgi.root_path     = bpath
        self.wsgi.instance_path = bpath

        self.wsgi.static_url_path = '/static'

        self.wsgi.template_folder = rpath('serve', 'views')
        self.wsgi.static_folder   = rpath('serve', 'assets')

    cache = property(lambda self: self._mem)
    wsgi  = property(lambda self: self._app)

    #*********************************************************************

    def register(self, *args, **kwargs):
        def do_apply(handler, alias):
            if alias not in self._reg:
                self._reg[alias] = handler

            if not hasattr(self, alias):
                setattr(self, alias, handler(self))

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    ######################################################################

    class Manager(Manageable):
        pass

    #*********************************************************************

    class ORM(Manageable):
        def initialize(self):
            self._cnx = None

            self._reg = {
                'field': {},
            }

            self.trigger('prepare')

        @property
        def cursor(self):
            if self._cnx:
                self._cnx = self.connect()

            return self._cnx

        ######################################################################

        def register_type(self, *args, **kwargs):
            def do_apply(handler, alias):
                if alias not in self._reg['field']:
                    self._reg['field'][alias] = handler

                return handler

            return lambda hnd: do_apply(hnd, *args, **kwargs)

        #*********************************************************************

        def field(self):
            pass

