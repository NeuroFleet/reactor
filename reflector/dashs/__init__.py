from reactor.reflector.shortcuts import *

################################################################################

@Prism.wsgi.route("/linked/")
def linked_view():
    return render_template('linked/homepage.html', widgets=[
        ('lg-8', [
            Prism.ui.widget('custom/visits', title="Visits", heading="Based on a three months data", data=[
                
            ], flags=dict(sizes=('sm-3','xs-6'), items=[
                dict(title="Total Traffic", value=24541,    icon='users',       color='green'),
                dict(title="Unique Users",  value=14778,    icon='bolt',        color='red'),
                dict(title="Revenue",       value=3583.18,  icon='plus-square', color='green'),
                dict(title="Total Sales",   value=59871.12, icon='user',        color='red'),
            ])),
            Prism.ui.widget('custom/traffic', title="Traffic Sources", heading="One month tracking", fields=[
                dict(label="Source", header="source-col-header"),
                dict(label="Amount"),
                dict(label="Change"),
                dict(label="Percent.,%", header="hidden-xs"),
                dict(label="Target"),
                dict(label="Trend", header="chart-col-header hidden-xs"),
            ], rows=[
                
            ]),
        ]), ('lg-4', [
            Prism.ui.widget('custom/news', title="Newsfeed", heading="all combined", entries=[
                dict(
                    sender=dict(full="Tikhon Laninga", mugshot='img/2.jpg'),
                    receiver='',
                    when='4 min',
                    content="Hey Sam, how is it going? But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born",
                ),
            ]),
            Prism.ui.widget('custom/chat', title="Instant Talk", heading="with John Doe", messages=[
                dict(
                    sender=dict(full="Tikhon Laninga", mugshot='img/2.jpg'),
                    receiver='',
                    when='4 min',
                    content="Hey Sam, how is it going? But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born",
                ),
            ]),
        ]),
    ])

#*******************************************************************************

@Prism.wsgi.route("/linked/graph")
def linked_graph_view():
    dash = [
        ('md-3', [
            Prism.ui.widget('handy/badge', title="", heading="On your website", buttons=[
                dict(icon='clock-o',  link='#'),
                dict(icon='bell',     link='#'),
                dict(icon='calendar', link='#'),
            ], icon='user', css_class='info'),
        ]),
    ]

    for key in (
        #'connector','console',
        'devops','infosec',
        'linked','linguist',
        'explor','hobbit',
    ):
        dash += [('md-3', [
            Prism.ui.widget('handy/counter', title="Triples in", heading="'%s' domain" % key.capitalize(),
                value   = 0,
                #target  = 'pages-messages.html',
            icon=key, css_class='default')
        ])]

    return render_template('linked/graph.html', widgets=dash+[
        #('md-4', [
        #    Prism.ui.widget('static/activity', title="Whatever"),
        #]), ('md-4', [
        #    Prism.ui.widget('static/visitors', title="Whatever"),
        #]), ('md-4', [
        #    Prism.ui.widget('static/projects', title="Whatever"),
        #]), ('md-8', [
        #    Prism.ui.widget('static/sales', title="Whatever"),
        #]), ('md-4', [
        #    Prism.ui.widget('static/purchase', title="Whatever"),
        #]),
    ])

#*******************************************************************************

@Prism.wsgi.route("/linked/query")
def linked_query_view():
    return render_template('linked/query.html', widgets=[
        ('md-3', [
            Prism.ui.widget('handy/counter', title="New messages", heading="In your mailbox",
                value   = 48,
                target  = 'pages-messages.html',
            icon='envelope', css_class='default'),
        ]), ('md-3', [
            Prism.ui.widget('handy/counter', title="Registred users", heading="On your website",
                value   = 375,
                target  = 'pages-address-book.html',
            icon='user', css_class='success'),
        ]), ('md-3', [
            Prism.ui.widget('handy/badge', title="", heading="On your website", buttons=[
                dict(icon='clock-o',  link='#'),
                dict(icon='bell',     link='#'),
                dict(icon='calendar', link='#'),
            ], icon='user', css_class='info'),
        ]), ('md-4', [
            Prism.ui.widget('static/activity', title="Whatever"),
        ]), ('md-4', [
            Prism.ui.widget('static/visitors', title="Whatever"),
        ]), ('md-4', [
            Prism.ui.widget('static/projects', title="Whatever"),
        ]), ('md-8', [
            Prism.ui.widget('static/sales', title="Whatever"),
        ]), ('md-4', [
            Prism.ui.widget('static/purchase', title="Whatever"),
        ]),
    ])

#*******************************************************************************

@Prism.wsgi.route("/linked/search", methods=['GET','POST'])
def linked_search_view():
    return render_template('linked/search.html', widgets=[
        ('md-3', [
            Prism.ui.widget('handy/counter', title="New messages", heading="In your mailbox",
                value   = 48,
                target  = 'pages-messages.html',
            icon='envelope', css_class='default'),
        ]), ('md-3', [
            Prism.ui.widget('handy/counter', title="Registred users", heading="On your website",
                value   = 375,
                target  = 'pages-address-book.html',
            icon='user', css_class='success'),
        ]), ('md-3', [
            Prism.ui.widget('handy/badge', title="", heading="On your website", buttons=[
                dict(icon='clock-o',  link='#'),
                dict(icon='bell',     link='#'),
                dict(icon='calendar', link='#'),
            ], icon='user', css_class='info'),
        ]), ('md-4', [
            Prism.ui.widget('static/activity', title="Whatever"),
        ]), ('md-4', [
            Prism.ui.widget('static/visitors', title="Whatever"),
        ]), ('md-4', [
            Prism.ui.widget('static/projects', title="Whatever"),
        ]), ('md-8', [
            Prism.ui.widget('static/sales', title="Whatever"),
        ]), ('md-4', [
            Prism.ui.widget('static/purchase', title="Whatever"),
        ]),
    ])

