#-*- coding: utf-8 -*-

import quepy

################################################################################

class BaseProvider(object):
    manager = property(lambda self: self._mgr)
    name    = property(lambda self: self._key)

    def __init__(self, mgr, name, *args, **kwargs):
        self._mgr = mgr
        self._key = name
        
        self._reg = {}
        
        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

    def ns(self, key, auto=False, default=None):
        if key in self._reg:
            return self._reg[key]
        elif auto:
            resp = quepy.install("neurochip.semantic.%s" % key)

            self._reg[key] = resp

            return resp
        else:
            return default
    
################################################################################

class BaseIntent(object):
    def __init__(self, prn, raw, *args, **kwargs):
        self._prn = prn
        self._raw = raw
        
        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

    provider = property(lambda self: self._prn)
    message  = property(lambda self: self._raw)

    manager  = property(lambda self: self.provider.manager)
    text     = property(lambda self: self.message['text'])

    @property
    def channel(self):
        return self.manager.slack_client.server.channels.find(self.message['channel'])

    def ns(self, *args, **kwargs): return self.provider.ns(*args, **kwargs)

################################################################################

class BaseResults(object):
    def __init__(self, prnt, raw, *args, **kwargs):
        self._won = prnt
        self._raw = raw
        
        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

    wonder  = property(lambda self: self._won)
    raw     = property(lambda self: self._raw)

    channel = property(lambda self: self.wonder.channel)

    def __getitem__(self, key,default=None):
        return self.raw.get(key, default)

