from reactor.shell import *

#*******************************************************************************

#apath = lambda *x: rpath('..','amber',*x)

################################################################################

@cli.command('run:spider')
@click.option('-s','--spider', multiple=True)
def crawling(spider):
    pth = Reactor.fs.resolve('program','bot')

    if not os.path.exists(pth):
        os.system('git clone %s %s' % ('https://github.com/scality/S3.git',pth))

        os.chdir(pth)

        os.system('npm init')
    else:
        os.chdir(pth)

    os.system('npm start')

#*******************************************************************************

@cli.command('run:bot')
@click.option('-a', '--adapter', default='slack')
@click.option('-n', '--name', default='alex')
@click.option('-s', '--script', multiple=True)
#@click.option('--queue', '-q', multiple=True)
def sidekick(adapter,name,script):
    if not os.path.exists(Reactor.fs.resolve('program','bot','scripts')):
        os.system('mkdir -p %s' % Reactor.fs.resolve('program','bot','scripts'))

    os.chdir(Reactor.fs.resolve('program','bot'))

    custom,extern,target = [
        "bookmark","wordpress"
    ],[
        ('hubot-'+x,'*') for x in (
            "help","diagnostics","heroku-keepalive",
            "cron","pubsub","mongodb-brain",
            "pugme","shipit","rules",
         )+(
            "google-images","google-translate",
            "maps","tweets","youtube","wikipedia"
         )+(
            "asana",
         )+(
            "analytics","vtr-scripts"
         )+(
            "reddit","stackoverflow-search"
         )
    ],[]

    for entry in script:
        if entry.startswith('hubot-'):
            extern.append(entry)
        else:
            custom.append(entry)

    for entry in custom:
        if not Reactor.fs.resolve('program','bot','scripts','%s.coffee' % entry):
            pass
        else:
            target.append(entry)

    Reactor.fs.write_json(Reactor.fs.resolve('program','bot','hubot-scripts.json'),    target)
    Reactor.fs.write_json(Reactor.fs.resolve('program','bot','external-scripts.json'), extern)

    os.system('node_modules/.bin/hubot -a %s -n %s' % (adapter,name))

