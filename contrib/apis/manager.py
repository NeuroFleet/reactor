from reactor.libs import *
from reactor.aliases import *

from reactor.abstract.meta import *
from reactor.abstract.core import *

#*******************************************************************************

import requests

################################################################################

class BaseAPI(Mutable):
    def __init__(self, parent, tenant, *args, **kwargs):
        self._prnt = parent
        self._user = tenant

        self._mdl = {}

        self.trigger('initialize', *args, **kwargs)

    parent = property(lambda self: self._prnt)
    tenant = property(lambda self: self._user)

    models = property(lambda self: self._mdl)

    #***************************************************************************

    config = property(lambda self: self._cfg)
    vault  = property(lambda self: self._sec)

    #***************************************************************************

    def register(self, alias, handler):
        if alias not in self._mdl:
            self._mdl[alias] = handler

        return self.models.get(alias, None)

#*******************************************************************************

@Reactor.extend('social')
class SocialManager(Reactor.Extension):
    ONTOLOGY = {
        'people': [
            ('google',    'contact'),
            ('facebook',  'user'),
            ('instagram', 'user'),
            #('linkedin',  'person'),
        ],
        'brands': [
            ('facebook',  'page'),
            #('linkedin',  'company'),
        ],
        'events': [
            #('facebook',  'event'),
        ],
        'places': [
            #('facebook',  'place'),
        ],
    }

    #***************************************************************************

    def initialize(self, **config):
        self._reg = {}
        self._res = {}
        self._bkn = {}

    ############################################################################

    registry  = property(lambda self: self._reg)
    resources = property(lambda self: self._res)

    #***************************************************************************

    def register_api(self, *args, **kwargs):
        def do_apply(handler, alias):
            if alias not in self._reg:
                self._reg[alias] = handler

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #***************************************************************************

    def register_auth(self, *args, **kwargs):
        def do_apply(handler, narrow, provider, **links):
            if narrow not in self._bkn:
                mode = links.get('mode','basic'),

                if 'mode' in links: del links['mode']

                entry = dict(
                    narrow   = narrow,
                    prefix   = 'API_%s_' % narrow.upper(),

                    provider = provider,
                    handler  = handler,

                    links    = links,
                    vault    = {},
                )

                for key in os.environ.keys():
                    if key.startswith(entry['prefix']):
                        nrw = key.replace(entry['prefix'], '')

                        entry['vault'][nrw.lower()] = os.environ[key]

                self._bkn[narrow] = entry

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #***************************************************************************

    def register_resource(self, *args, **kwargs):
        def do_apply(handler, api, key):
            if api not in self._res:
                self._res[api] = {}

            if key not in self._res[api]:
                self._res[api][key] = handler

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #***************************************************************************

    def register_filesystem(self, *args, **kwargs):
        def do_apply(handler, api, key):
            if api not in self._res:
                self._vfs[api] = {}

            if key not in self._res[api]:
                self._vfs[api][key] = handler

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    ############################################################################

    def by_login(self, pseudo):
        from django.contrib.auth.models import User

        user = User.objects.get(username=pseudo)

        return self.by_user(user)

    #***************************************************************************

    def by_user(self, user):
        resp = {}

        for backend in user.social_auth.all():
            key = backend.provider

            for word in ('-oauth2','-oauth'):
                while word in key:
                    key = key.replace(word,'')

            if key in self._reg:
                hnd = self._reg[key]

                resp[key] = hnd(self, user, backend)

        return resp

    #***************************************************************************

    def topics(self, pseudo):
        cnx = self.by_login(pseudo)

        res = {}

        for onto in self.ONTOLOGY:
            res[onto] = []

            for api,sch in self.ONTOLOGY[onto]:
                if api in cnx:
                    for item in cnx[api].listing(sch):
                        res[onto].append(item)

        return res

    #***************************************************************************

    def get_backends(self, stt, *extra):
        resp = [
            #'social_core.backends.aol.AOLOpenId',
            #'social_core.backends.belgiumeid.BelgiumEIDOpenId',
            #'social_core.backends.fedora.FedoraOpenId',
            #'social_core.backends.launchpad.LaunchpadOpenId',
            #'social_core.backends.livejournal.LiveJournalOpenId',
            #'social_core.backends.open_id.OpenIdAuth',
            #'social_core.backends.persona.PersonaAuth',
            #'social_core.backends.steam.SteamOpenId',
            #'social_core.backends.suse.OpenSUSEOpenId',
            #'social_core.backends.ubuntu.UbuntuOpenId',
            #'social_core.backends.yahoo.YahooOpenId',
            #'social_core.backends.yandex.YandexOpenId',
        ]

        for entry in self._bkn.values():
            if len(entry['vault']):
                for key,value in entry['handler'](entry['vault']):
                    name = 'SOCIAL_AUTH_%s_%s' % (entry['narrow'].upper(), key.upper())

                    stt[name] = value

                resp.append('social_core.backends.%s.%s' % (entry['narrow'].lower(), provider))

        return tuple(resp) + tuple(extra)

    ############################################################################

    class API(BaseAPI):
        def initialize(self, backend):
            self._prvd = backend

            self._cfg = {}
            self._sec = {}

            self.trigger('prepare')

        backend  = property(lambda self: self._prvd)
        alias    = property(lambda self: self.backend.provider)

        creds    = property(lambda self: self.backend.extra_data)

        #***********************************************************************

        def http_req(self, method, url, params={}, data={}, headers={}):
            hnd = getattr(requests, method.lower(), None)

            if callable(hnd):
                resp = hnd(url, params=params, data=data, headers=headers)

                try:
                    return resp.json()
                except Exception,ex:
                    print url,resp.status_code,resp.text

                    raise ex
            else:
                raise Exception("Unsupported HTTP method '%s' for : %s" % (method,url))

                return None

        def http_put(self, *args, **kwargs):  return self.request('PUT', *args, **kwargs)
        def http_get(self, *args, **kwargs):  return self.request('GET', *args, **kwargs)
        def http_post(self, *args, **kwargs): return self.request('POST', *args, **kwargs)
        def http_del(self, *args, **kwargs):  return self.request('DELETE', *args, **kwargs)

        #***********************************************************************

        def narrow(self, alias, *args, **kwargs):
            resp = None

            if alias in self.models:
                item = self.models[alias].narrow(self, *args, **kwargs)

                resp = self.models[alias].from_request(self, item)

            return resp

        def listing(self, alias, *args, **kwargs):
            resp = []

            if alias in self.models:
                for item in self.models[alias].listing(self, *args, **kwargs):
                    entry = self.models[alias].from_request(self, item)

                    resp.append(item)

            return resp

    #***************************************************************************

    class Resource(Mutable):
        @classmethod
        def from_request(cls, api, raw):
            nrw = {}
            dtn = {}

            for field in raw:
                if field in cls.NARROW:
                    nrw[field] = raw[field]
                else:
                    dtn[field] = raw[field]


        def __init__(self, api, nrw, dtn={}):
            self._prn = api
            self._nrw = nrw
            self._dtn = dtn

        parent = property(lambda self: self._prn)
        narrow = property(lambda self: self._nrw)
        data   = property(lambda self: self._dtn)

        def __getattr__(self, key, default=None):
            if key in self._nrw:
                return self._nrw[key]
            elif key in self._dtn:
                return self._dtn[key]
            else:
                return default

    ############################################################################

    class Filesystem(Mutable):
        @classmethod
        def from_request(cls, api, raw):
            nrw = {}
            dtn = {}

            for field in raw:
                if field in cls.NARROW:
                    nrw[field] = raw[field]
                else:
                    dtn[field] = raw[field]


        def __init__(self, api, nrw, dtn={}):
            self._prn = api
            self._nrw = nrw
            self._dtn = dtn

        parent = property(lambda self: self._prn)
        narrow = property(lambda self: self._nrw)
        data   = property(lambda self: self._dtn)

        ########################################################################

        def resolve(self, *path):
            return rpath('..', *path)

        #***********************************************************************

        def read(self, *path):
            return open(self.resolve(*path)).read()

        #***********************************************************************

        def write(self, path, *content):
            resp = "\n".join(content)

            with open(path, 'w+') as f:
                f.write(resp)

            return resp

        #***********************************************************************

        def render(self, path, **context):
            pass

        ########################################################################

        def read_json(self, *path):
            return json.loads(self.read(*path))

        #***********************************************************************

        def write_json(self, path, data):
            raw = json.dumps(data)

            for src,dest in [
                ('u"','"'),
            ]:
                while src in raw:
                    raw = raw.replace(src,dest)

            return self.write(path, raw)

        ########################################################################

        def read_yaml(self, *path):
            return yaml.loads(self.read(*path))

        #***********************************************************************

        def write_yaml(self, path, data):
            return self.write(path, yaml.dumps(data))

