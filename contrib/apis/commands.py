from reactor.shell import *

################################################################################

@cli.command('serve:api')
@click.option('--queue', '-q', multiple=True)
def serve_blueprint(queue):
    pass

#*******************************************************************************

@cli.command('serve:rpc')
@click.option('--queue', '-q', multiple=True)
def serve_xmlrpc(queue):
    pass

