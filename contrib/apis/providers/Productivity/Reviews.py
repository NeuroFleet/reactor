from reactor.helpers import *

################################################################################

@Reactor.social.register_auth('reddit', 'RedditOAuth2',
    register_url='',
    overview_url='',
mode='oauth2')
def detect_reddit(cfg):
    yield 'KEY', cfg.get('key', 'jiKcMgxZ2S6Xvg')
    yield 'SECRET', cfg.get('pki', '9o4kH2dM3Blf-sDGwNQjMV78GRA')

    yield 'AUTH_EXTRA_ARGUMENTS', {
        'duration': 'permanent',
    }

#*******************************************************************************

@Reactor.social.register_auth('stackoverflow', 'StackoverflowOAuth2',
    register_url='',
    overview_url='',
mode='oauth2')
def detect_stackoverflow(cfg):
    yield 'KEY',     cfg.get('cid', 'xxxxxx')
    yield 'SECRET',  cfg.get('pki', 'xxxxxx')
    yield 'API_KEY', cfg.get('key', 'xxxxxx')

#*******************************************************************************

@Reactor.social.register_auth('qiita', 'QiitaOAuth2',
    register_url='',
    overview_url='',
mode='oauth2')
def detect_mailru(cfg):
    yield 'KEY', cfg.get('key', 'aab863dea38f889f4085048667f62b50')
    yield 'SECRET', cfg.get('pki', '507a4b7edcb6fb3194f4d01c24f207e0')

