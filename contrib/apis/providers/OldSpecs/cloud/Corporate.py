from reactor.shortcuts import *

################################################################################

@Reactor.social.register_api('google')
class Google(Reactor.social.API):
    def prepare(self):
        self.register('contact', GoogleContact)

    def request(self, method, url, params={}, payload=None, headers={}):
        url = 'https://www.googleapis.com' + url

        params['access_token'] = self.creds['access_token']

        return self.http_req(method, url, params, payload, headers)

#*******************************************************************************

@Reactor.social.register_resource('google','contact')
class GoogleContact(Reactor.social.Resource):
    @classmethod
    def listing(cls, api, **query):
        resp = api.http_get('/plus/v1/people/%s/people/visible' % 'me')

        if 'items' in resp:
            for person in resp['items']:
                #Reactor.wamp.publish(u"reactor.perso.social.knows", 'google', profile, person, RATINGs[1])

                yield person
        else:
            print resp

    @classmethod
    def narrow(cls, api, **uid):
        pass

    NARROW = ['id']

    def prepare(self):
        pass

