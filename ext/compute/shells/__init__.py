from reactor.libs import *
from reactor.aliases import *

from reactor.abstract.meta import *
from reactor.abstract.core import *

#*******************************************************************************

import execjs

################################################################################

@Reactor.extend('sh')
class Shell(Reactor.Syntax):
    def prepare(self):
        pass

    #***************************************************************************

    def escape(self, value):
        chars = (' ','"',"'", '!')

        if reduce(operator.or_, [(x in value) for x in chars]):
            return '"%s"' % value.replace('"', '\\"').replace("'", "\\'").replace('!', '\\!')
        else:
            return value

    ############################################################################

    def evaluate(self, *stmt):
        cmd = ' '.join([self.escape(x) for x in stmt])

        os.system(cmd)

    #***************************************************************************

    def script(self, source, **context):
        pass

#*******************************************************************************

@Reactor.extend('py')
class Python(Reactor.Syntax):
    def initialize(self, **config):
        pass

    ############################################################################

    def xxxxxx(self):
        execjs.eval("'red yellow blue'.split(' ')") # ['red', 'yellow', 'blue']

    #***************************************************************************

    def yyyyyy(self):
        ctx = execjs.compile("""
        ...     function add(x, y) {
        ...         return x + y;
        ...     }
        ... """)

        ctx.call("add", 1, 2) # 3

#*******************************************************************************

@Reactor.extend('js')
class JavaScript(Reactor.Syntax):
    def initialize(self, **config):
        pass

    ############################################################################

    def manifest(self, path, alias, *deps, **kwargs):
        cnt = {
            "name": 'neurochip-%s' % alias,
            "description": kwargs.get('summary', "A modular hub for the new Information Age."),
            "version": kwargs.get('version', "0.0.1"),
            "author": kwargs.get('author', "Uchikoma bot"),
            "license": "MIT",
            "dependencies": {
                "async": "^1.4.2",
                "autobahn": "^0.11.2",
                "lodash": "^3.10.1",
                "multer": "^1.0.3",
                "mongodb-runner": "*",

                "busboy": "^0.2.9",
                "connect-busboy": "0.0.2",
                "easyimage": "^2.0.3",
                "express": "^4.13.3",
                "express-handlebars": "^2.0.1",
            }
        }

        for kv in deps:
            if type(kv) not in [tuple,set,list]:
                kv = (kv, '*')

            cnt['dependencies'][kv[0]] = kv[1]

        Reactor.fs.write(path, self.dump(cnt))

    ############################################################################

    def escape(self, data):
        return data

    #***************************************************************************

    def dump(self, data): return json.dumps(self.escape(data), pretty=True)
    def load(self, data): return json.loads(self.parse(data))

    #***************************************************************************

    def parse(self, data):
        return data

    ############################################################################

    def xxxxxx(self):
        execjs.eval("'red yellow blue'.split(' ')") # ['red', 'yellow', 'blue']

    #***************************************************************************

    def yyyyyy(self):
        ctx = execjs.compile("""
        ...     function add(x, y) {
        ...         return x + y;
        ...     }
        ... """)

        ctx.call("add", 1, 2) # 3

################################################################################

@Reactor.extend('sql')
class SimpleQueryLanguage(Reactor.Syntax):
    def initialize(self, **config):
        pass

    ############################################################################

    def xxxxxx(self):
        execjs.eval("'red yellow blue'.split(' ')") # ['red', 'yellow', 'blue']

    #***************************************************************************

    def yyyyyy(self):
        ctx = execjs.compile("""
        ...     function add(x, y) {
        ...         return x + y;
        ...     }
        ... """)

        ctx.call("add", 1, 2) # 3

#*******************************************************************************

@Reactor.extend('cypher')
class CypherLang(Reactor.Syntax):
    def initialize(self, **config):
        pass

    ############################################################################

    def xxxxxx(self):
        execjs.eval("'red yellow blue'.split(' ')") # ['red', 'yellow', 'blue']

    #***************************************************************************

    def yyyyyy(self):
        ctx = execjs.compile("""
        ...     function add(x, y) {
        ...         return x + y;
        ...     }
        ... """)

        ctx.call("add", 1, 2) # 3

