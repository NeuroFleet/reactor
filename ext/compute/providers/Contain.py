from reactor.helpers import *

################################################################################

@Reactor.exe.register_backend('docker')
class DockerRemote(Reactor.exe.Backend):
    def prepare(self):
        pass

    #***************************************************************************

    def scheduler(self):
        pass

    #***************************************************************************

    def worker(self):
        pass

    #***************************************************************************

    def pipeline(self):
        pass

################################################################################

@Reactor.exe.register_backend('mesos')
class Mesosphere(Reactor.exe.Backend):
    def prepare(self):
        pass

    #***************************************************************************

    def scheduler(self):
        pass

    #***************************************************************************

    def worker(self):
        pass

    #***************************************************************************

    def pipeline(self):
        pass

