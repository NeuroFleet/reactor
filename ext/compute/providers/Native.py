from reactor.helpers import *

################################################################################

@Reactor.exe.register_backend('rq')
class RedisQueue(Reactor.exe.Backend):
    def prepare(self):
        pass

    #***************************************************************************

    def scheduler(self):
        pass

    #***************************************************************************

    def worker(self):
        pass

    #***************************************************************************

    def pipeline(self):
        pass

################################################################################

@Reactor.exe.register_backend('wamp')
class WampBalancer(Reactor.exe.Backend):
    def prepare(self):
        pass

    #***************************************************************************

    def scheduler(self):
        pass

    #***************************************************************************

    def worker(self):
        pass

    #***************************************************************************

    def pipeline(self):
        pass

