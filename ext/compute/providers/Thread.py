from reactor.helpers import *

################################################################################

from multiprocessing import Pool,Queue,Process
from multiprocessing.managers import BaseManager

#*******************************************************************************

@Reactor.exe.register_backend('thread')
class MultiProcessing(Reactor.exe.Backend):
    manager = property(lambda self: self._mgr)
    proxy   = property(lambda self: self._prx)

    def prepare(self, host=None, port=50000):
        if host is None:
            self._mgr = BaseManager(address=('',port), authkey=auth)
        else:
            self._mgr = BaseManager(address=(host,port), authkey=auth)

    #***************************************************************************

    def scheduler(self):
        hnd = self.manager.get_server()

        hnd.serve_forever()

    #***************************************************************************

    def worker(self):
        self.manager.connect()

        with Pool(processes=4) as pool:         # start 4 worker processes
            result = pool.apply_async(f, (10,)) # evaluate "f(10)" asynchronously in a single process
            print(result.get(timeout=1))        # prints "100" unless your computer is *very* slow

            print(pool.map(f, range(10)))       # prints "[0, 1, 4,..., 81]"

            it = pool.imap(f, range(10))
            print(next(it))                     # prints "0"
            print(next(it))                     # prints "1"
            print(it.next(timeout=1))           # prints "4" unless your computer is *very* slow

            result = pool.apply_async(time.sleep, (10,))
            print(result.get(timeout=1))        # raises multiprocessing.TimeoutError

    #***************************************************************************

    def pipeline(self):
        self.manager.connect()

        with Pool(processes=4) as pool:         # start 4 worker processes
            pass

################################################################################

#*******************************************************************************

@Reactor.exe.register_backend('dask')
class DaskClient(Reactor.exe.Backend):
    def prepare(self):
        pth = Reactor.fs.resolve('program','dsk')

        if not os.path.exists(pth):
            os.shell('mkdir -p %s' % pth)

        os.chdir(pth)

    #***************************************************************************

    def scheduler(self):
        loc = '%s:%s' % (host,str(port))

        Reacto.cli.shell('dask-scheduler', loc, '--nprocs', 8, '--nthreads', 1)

    #***************************************************************************

    def worker(self):
        loc = '%s:%s' % (host,str(port))

        Reactor.cli.shell('dask-worker', loc, '--nprocs', 8, '--nthreads', 1)

    #***************************************************************************

    def pipeline(self):
        pass

