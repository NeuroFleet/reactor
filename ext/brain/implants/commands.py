from reactor.shell import *

################################################################################

@cli.command('consume:amqp')
@click.option('--queue', '-q', multiple=True)
def amqp_consumer(queue):
    pass

#*******************************************************************************

@cli.command('consume:mqtt')
@click.option('--queue', '-q', multiple=True)
def mqtt_consumer(queue):
    pass

#*******************************************************************************

@cli.command('consume:sock')
@click.option('--queue', '-q', multiple=True)
def sock_consumer(queue):
    pass

################################################################################

@cli.command('consume:wamp')
@click.argument('endpoint', default=u"ws://apis.uchikoma.faith:8000/io-sockets")
@click.option('--realm', '-r', default="reactor")
@click.option('--prefix', '-p', default="")
@click.option('--middlware', '-mw', multiple=True)
@click.option('--suffix', '-s', default="")
def wamp_consumer(endpoint, realm, prefix, middlware, suffix):
    pool = eventlet.GreenPool()

    for path in middlware:
        pool.spawn_n(process_middleware, pool, endpoint, realm, prefix, path, suffix)

    pool.waitall()

#*******************************************************************************

@cli.command('consume:xmpp')
@click.option('--queue', '-q', multiple=True)
def xmpp_consumer(queue):
    pass

