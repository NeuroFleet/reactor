from reactor.libs import *
from reactor.aliases import *

from reactor.abstract.meta import *
from reactor.abstract.core import *

#*******************************************************************************

import mongoengine as MongoDB
import pymongo

#*******************************************************************************

from bulbs.model import Node             as Neo4j_Node
from bulbs.model import Relationship     as Neo4j_Edge

from bulbs       import property         as Neo4j_Prop
from bulbs.utils import current_datetime as Neo4j_Now

################################################################################

@Reactor.extend('ORM')
class Django_ORM(Reactor.ObjectMapper):
    def prepare(self, **config):
        pass

    #***************************************************************************

    def connect(self, **creds):
        pass

################################################################################

@Reactor.extend('MONGO')
class MongoEngine(Reactor.ObjectMapper):
    def prepare(self, **config):
        self._crs = None

        #from reactor.webapp import settings as stt

        #for alias,config in stt.MONGODB_DATABASES.iteritems():
        #    cnx = MongoDB.connect(alias, host=config['LINK'])

        #    if self._crs is None:
        #        self._crs = cnx

    #***************************************************************************

    def connect(self, **creds):
        return self._crs # pymongo.Connection('mongodb://localhost:27017')

################################################################################

@Reactor.extend('GRAPH')
class BulbsFlow(Reactor.ObjectMapper):
    Node = Neo4j_Node
    Edge = Neo4j_Edge

    Now  = Neo4j_Now

    def prepare(self, **config):
        self._reg = {
            'nodes': {},
            'edges': {},
        }
        self._map = {
            'str':     str,
            'string':  str,
            'unicode': unicode,
            'text':    unicode,
        }
        self._std = {
            str:      Neo4j_Prop.String,
            unicode:  Neo4j_Prop.String,

            int:      Neo4j_Prop.Integer,
            long:     Neo4j_Prop.Integer,

            #date:     Neo4j_Prop.Date,
            datetime: Neo4j_Prop.DateTime,
        }

        self._cfg = None
        self._cyf = None

    #***************************************************************************

    @property
    def neo4cfg(self):
        from bulbs.config import Config

        if self._cfg is None:
            from reactor.webapp import settings as stt

            cfg = stt.NEO4J_DATABASES['scubadev']

            if 'LINK' in cfg:
                lnk = urlparse(cfg['LINK'])

                cfg['HOST']     = lnk.hostname
                cfg['PORT']     = lnk.port
                cfg['USERNAME'] = lnk.username
                cfg['PASSWORD'] = lnk.password

            self._cfg = Config('http://%(HOST)s:%(PORT)s/db/data/' % cfg, cfg['USERNAME'], cfg['PASSWORD'])

        return self._cfg

    cypher = property(lambda self: self._cyf)

    #***************************************************************************

    def connect(self, **creds):
        from bulbs.neo4jserver.cypher import Cypher

        self._cyf = Cypher(self.neo4cfg)

        from bulbs.neo4jserver import Graph

        return Graph(self.neo4cfg)

    #***************************************************************************

    nodes = property(lambda self: self._reg['nodes'])
    edges = property(lambda self: self._reg['edges'])

    E     = property(lambda self: self.proxy.E)
    V     = property(lambda self: self.proxy.V)

    ############################################################################

    def field(self, target):
        resp = target

        while resp in self._map:
            resp = self._map[resp]

        if resp in self._std:
            resp = self._std[resp]
        else:
            resp = None

        return resp

    #***************************************************************************

    def register_node(self, *args, **kwargs):
        def do_apply(handler, alias):
            setattr(handler, 'element_type', alias)

            if alias not in self._reg['nodes']:
                self._reg['nodes'][alias] = self.proxy.add_proxy(alias, handler)

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #***************************************************************************

    def register_node(self, *args, **kwargs):
        def do_apply(handler, alias):
            setattr(handler, 'label', alias)

            if alias not in self._reg['edges']:
                self._reg['edges'][alias] = self.proxy.add_proxy(alias, handler)

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    ############################################################################

    def node(self, alias):
        hnd = None

        if alias in self._reg['nodes']:
            hnd = self._reg['nodes']
        else:
            pass

        if hnd is not None:
            pass # hnd

        return hnd

    #***************************************************************************

    def edge(self, alias):
        hnd = None

        if alias in self._reg['edges']:
            hnd = self._reg['edges']
        else:
            pass

        if hnd is not None:
            pass # hnd

        return hnd

    #***************************************************************************

    #def process(self):
    #    james = self.node('people').create(name="James")
    #    julie = self.node('people').create(name="Julie")
    #
    #    self.edge('knows').create(james, julie)

################################################################################
################################################################################

import rdflib
#import owlready
#import rdfalchemy

#*******************************************************************************

@Reactor.extend('RDF')
class RdfLib(Reactor.Extension):
    def initialize(self, **settings):
        pass

#*******************************************************************************

@Reactor.extend('OWL')
class OwL(Reactor.Extension):
    def initialize(self, **settings):
        pass

#*******************************************************************************

@Reactor.extend('ALCHEMY')
class RdfAlchemy(Reactor.ObjectMapper):
    def prepare(self, **config):
        pass

    #***************************************************************************

    def connect(self, **creds):
        pass

