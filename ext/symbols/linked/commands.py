from reactor.shell import *

#*******************************************************************************

apath = lambda *x: rpath('..','amber',*x)

################################################################################

class JS_App(Mutable):
    def initialize(self, path):
        pass

    def run(self, *args, **kwargs):
        pass

#*******************************************************************************

class JS_Ext(Mutable):
    def dependencies(self):
        yield "node-rules", "^3.1.0"
        yield "rdfstore", "^0.9.17"

################################################################################

class Reasoning(JS_Ext):
    def dependencies(self):
        yield "node-rules", "^3.1.0"

#*******************************************************************************

class Inference(JS_Ext):
    def dependencies(self):
        yield "rdfstore", "^0.9.17"

################################################################################

class MultiDaemon(JS_App):
    def dependencies(self):
        yield "smtp-server", "^1.17.0"
        yield "native-dns", "^0.7.0"

    def configure(self):
        pth = Reactor.fs.resolve('amber','servers','ldp.json')

        cfg = {
            "title":       "Reactor - Endpoints - LDF",
            "baseURL":     "http://ldf.uchikoma.faith/",
            "datasources": {}
        }

        Reactor.fs.write_json(pth, cfg)

    def lunch(self):
        Reactor.sh.run('')

#*******************************************************************************

class PerceptionField(JS_App):
    def dependencies(self):
        yield "opencv", "^6.0.0"
        yield "speakable", "^0.3.0"
        yield "say", "^0.11.0"

    def configure(self):
        pth = Reactor.fs.resolve('amber','servers','ldp.json')

        cfg = {
            "title":       "Reactor - Endpoints - LDF",
            "baseURL":     "http://ldf.uchikoma.faith/",
            "datasources": {}
        }

        Reactor.fs.write_json(pth, cfg)

    def lunch(self):
        Reactor.sh.run('')

################################################################################

class LinkedDataFragments(JS_App):
    def dependencies(self):
        yield "smtp-server", "^1.17.0"
        yield "native-dns", "^0.7.0"
        yield "node-rules", "^3.1.0"
        yield "rdfstore", "^0.9.17"
        yield "busboy", "^0.2.9"
        yield "connect-busboy", "0.0.2"
        yield "easyimage", "^2.0.3"
        yield "express", "^4.13.3"
        yield "express-handlebars", "^2.0.1"
        yield "opencv", "^6.0.0"
        yield "speakable", "^0.3.0"
        yield "say", "^0.11.0"
        yield "parse-server", "*"
        yield "ldf-server", "*"
        yield "solid-server", "*"

    def configure(self):
        pth = Reactor.fs.resolve('amber','servers','ldp.json')

        cfg = {
            "title":       "Reactor - Endpoints - LDF",
            "baseURL":     "http://ldf.uchikoma.faith/",
            "datasources": {}
        }

        Reactor.fs.write_json(pth, cfg)

    def lunch(self):
        Reactor.sh.run('')

#*******************************************************************************

class Parse(JS_App):
    def dependencies(self):
        yield "solid-server", "*"

    def configure(self):
        pth = Reactor.fs.resolve('amber','servers','ldp.json')

        cfg = {
            "title":       "Reactor - Endpoints - LDF",
            "baseURL":     "http://ldf.uchikoma.faith/",
            "datasources": {}
        }

        Reactor.fs.write_json(pth, cfg)

    def lunch(self):
        Reactor.sh.run('')

#*******************************************************************************

class Solid(JS_App):
    def dependencies(self):
        yield "solid-server", "*"

    def configure(self):
        pth = Reactor.fs.resolve('amber','servers','ldp.json')

        cfg = {
            "title":       "Reactor - Endpoints - LDF",
            "baseURL":     "http://ldf.uchikoma.faith/",
            "datasources": {}
        }

        Reactor.fs.write_json(pth, cfg)

    def lunch(self):
        Reactor.sh.run('')

################################################################################

@cli.command('serve:ldf')
@click.option('--port', default=60012)
@click.option('--workers', default=4)
def linked_data_fragments(port,workers):
    sources = [
        #dict(
        #    type='hdt', path="data/dbpedia2014.hdt",
        #    name='dbpedia-hdt', title="DBpedia 2014",
        #    summary="DBpedia 2014 with an HDT back-end",
        #),
        dict(
            type='sparql', ep="http://dbpedia.restdesc.org/", ns="http://dbpedia.org",
            name='dbpedia-sparql', title="DBpedia 3.9 (Virtuoso)",
            summary="DBpedia 3.9 with a Virtuoso back-end",
        ),
    ]

    pth = Reactor.fs.resolve('amber','servers','ldf.json')

    cfg = {
        "title":       "Reactor - Endpoints - LDF",
        "baseURL":     "http://ldf.uchikoma.faith/",
        "datasources": {}
    }

    for entry in sources:
        item = {
            "title": entry['title'] or entry['name'].capitalize(),
            "type": entry['type'].lower().capitalize() + "Datasource",
            "description": entry.get('summary', ""),
        }

        if entry['type']=='hdt':
            item['settings'] = {
                "file": entry['path'],
            }
        elif entry['type']=='sparql':
            item['settings'] = {
                "endpoint":     entry.get('ep', ""),
                "defaultGraph": entry.get('ns', "http://dbpedia.org"),
            }

        cfg['datasources'][entry['name']] = item

    Reactor.fs.write_json(pth, cfg)

    os.system('ldf-server %s %d %d' % (pth,port,workers))

#*******************************************************************************

@cli.command('serve:ldp')
@click.option('--port', default=60011)
@click.option('--mount', default='/ldp')
@click.option('--path',  default='/mnt')
def solid_server(port,mount,path):
    for key,url in dict(
        warp="linkeddata/warp",
    ).iteritems():
        pth = Reactor.fs.resolve('tools',key)

        if not os.path.exists(pth):
            os.system('git clone https://github.com/%s.git %s' % (url,pth))


    os.system('solid start --port %s --mount %s --root %s --no-webid --data-browser' % (port,mount,path))
    #os.system('solid start --port %s --mount %s --root %s --no-webid --data-browser --file-browser %s' % (port,mount,path))

################################################################################

@cli.command('serve:doc')
@click.option('--port', default=60010)
@click.option('--app', default='reactor')
@click.option('--key', default='MasterKeyPhrase')
@click.option('--db', default='default')
def parse_server(port,app,key,link):
    if db in settings.MONGO_DATABASES:
        link = settings.MONGO_DATABASES[db]

        os.system('parse-server --appId %s --masterKey %s --databaseURI %s' % (app,key,link))
    else:
        raise Exception("Unknown MongoDB database : %s" % db)

#*******************************************************************************

@cli.command('serve:sql')
@click.option('--port', default=60010)
@click.option('--schema', default='default')
@click.option('--db', default='default')
def sand_man(port,app,key,link):
    if db in settings.DATABASES:
        link = settings.DATABASES[db]

        for src,dest in [
            ('postgres://',   'postgresql+psycopg2://'),
            ('postgresql://', 'postgresql+psycopg2://'),
        ]:
            if src in link:
                link = link.replace(src, dest)

        os.system('sandman2ctl -p %s -s %s -d %s -r' % (port,meta,link))
    else:
        raise Exception("Unknown SQL database : %s" % db)

