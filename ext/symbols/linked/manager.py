from reactor.libs import *
from reactor.aliases import *

from reactor.abstract.meta import *
from reactor.abstract.core import *

#*******************************************************************************

import rdflib
import surf

################################################################################

@Reactor.extend('SPARQL')
class SparQL(Reactor.Extension):
    def initialize(self, **settings):
        pass

################################################################################

@Reactor.extend('SURF')
class SuRF(Reactor.ObjectMapper):
    def prepare(self, **config):
        self.store = surf.Store(reader = 'sparql_protocol',
                           endpoint = 'http://dbpedia.org/sparql',
                           default_graph = 'http://dbpedia.org')

        self.session = surf.Session(self.store, {})

        self.session.enable_logging = False

    #***************************************************************************

    def xxxxxx(self):
        PhilCollinsAlbums = self.session.get_class(surf.ns.YAGO['PhilCollinsAlbums'])

        all_albums = PhilCollinsAlbums.all()

        print 'Phil Collins has %d albums on dbpedia' % len(all_albums)

        first_album = all_albums.first()
        first_album.load()

        for a in all_albums:
            if a.dbpedia_name:
                cvr = a.dbpedia_cover
                print '\tCover %s for "%s"' % (str(a.dbpedia_cover), str(a.dbpedia_name))

    #***************************************************************************

    def yyyyyy(self):
        # Create FoafPerson class:
        FoafPerson = session.get_class(surf.ns.FOAF.Person)
        # Create instance of FoafPerson class:
        john = session.get_resource("http://john.com/me", FoafPerson)
        # or simply like this
        john = FoafPerson("http://john.com/me")

