from reactor.shell import *

################################################################################

@cli.command('serve:dav')
@click.option('--queue', '-q', multiple=True)
def serve_webdav(queue):
    pass

#*******************************************************************************

@cli.command('serve:s3')
@click.option('--queue', '-q', multiple=True)
def serve_minio(queue):
    pass

################################################################################

@cli.command('serve:vfs')
@click.option('--port', default=60014)
#@click.option('--queue', '-q', multiple=True)
def serve_scality():
    pth = rpath('..','programs','scality')

    if not os.path.exists(pth):
        os.system('git clone %s %s' % ('https://github.com/scality/S3.git',pth))

        os.chdir(pth)

        os.system('npm init')
    else:
        os.chdir(pth)

    os.system('npm start')

