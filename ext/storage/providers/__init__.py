from . import Native

from . import HTTP
from . import SQL

from . import S3
from . import WebDAV

from . import BoxNet
from . import DropBox

if False:
    from dbfs import DatabaseFS  # this project
    from fs.expose import fuse   # Pyfilesystem 

    # A series of python commands to create the DBcursor object. 
    curcmd = '''
    import psycopg2, psycopg2.extras
    conn = psycopg2.connect(database='shop', user='foo', password='bar')
    conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    '''

    # SQL for getting the full listing of files (including subdirs)
    # in our virtual filesystem
    listcmd = '''
    SELECT
        id AS fid,
        'byname/'||lastname||'_'||firstname AS path
        length(comment) AS size
    FROM feedback;
    '''
    # SQL for moving file contents from and to the DF
    readcmd= "SELECT comment FROM feedback WHERE id=%s"
    writecmd="UPDATE feedback SET comment=%s WHERE id=%s"

    # Test it
    commentfs = DatabaseFS(curcmd,listcmd,readcmd,writecmd)
    fuse.mount(commentfs, '/mnt/customer_feedback')

