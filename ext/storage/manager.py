from reactor.libs import *
from reactor.aliases import *

from reactor.abstract.meta import *
from reactor.abstract.core import *

#*******************************************************************************

#import fs

################################################################################

class BaseProvider(Mutable):
    @classmethod
    def from_request(cls, api, raw):
        nrw = {}
        dtn = {}

        for field in raw:
            if field in cls.NARROW:
                nrw[field] = raw[field]
            else:
                dtn[field] = raw[field]


    def __init__(self, api, nrw, dtn={}):
        self._prn = api
        self._nrw = nrw
        self._dtn = dtn

    parent = property(lambda self: self._prn)
    narrow = property(lambda self: self._nrw)
    data   = property(lambda self: self._dtn)

#*******************************************************************************

class BaseFS(object):
    pass

#*******************************************************************************

class BaseFile(object):
    pass

################################################################################

@Reactor.extend('vfs')
class VirtualFileSystem(Reactor.Extension):
    def initialize(self, **config):
        pass

    #***************************************************************************

    def register_filesystem(self, *args, **kwargs):
        def do_apply(handler, api, key):
            if api not in self._res:
                self._vfs[api] = {}

            if key not in self._res[api]:
                self._vfs[api][key] = handler

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    ############################################################################

    def resolve(self, *path):
        return rpath('..', *path)

    #***************************************************************************

    def read(self, *path):
        return open(self.resolve(*path)).read()

    #***************************************************************************

    def write(self, path, *content):
        resp = "\n".join(content)

        with open(path, 'w+') as f:
            f.write(resp)

        return resp

    #***************************************************************************

    def render(self, path, **context):
        pass

    ############################################################################

    def read_json(self, *path):
        return json.loads(self.read(*path))

    #***************************************************************************

    def write_json(self, path, data):
        raw = json.dumps(data)

        for src,dest in [
            ('u"','"'),
        ]:
            while src in raw:
                raw = raw.replace(src,dest)

        return self.write(path, raw)

    ############################################################################

    def read_yaml(self, *path):
        return yaml.loads(self.read(*path))

    #***************************************************************************

    def write_yaml(self, path, data):
        return self.write(path, yaml.dumps(data))

