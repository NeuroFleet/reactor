from reactor.libs import *
from reactor.aliases import *

from reactor.abstract.meta import *
from reactor.abstract.core import *

#*******************************************************************************

import django_rq
from redis import Redis
from rq_scheduler import Scheduler

#*******************************************************************************

#Reactor.schedule.run_at(datetime(2020, 1, 1))()
#Reactor.schedule.run_at(datetime(2020, 1, 1, 3, 4))(foo, bar=baz)
#Reactor.schedule.within(timedelta(minutes=10))(tweet_id)
#Reactor.schedule.within(timedelta(hours=1))(tweet_id)
#Reactor.schedule.within(timedelta(days=1))(tweet_id)
#Reactor.schedule.every(interval=60, repeat=10)(arg1, arg2, foo='bar')
#Reactor.schedule.cron("0 0 * * 0", repeat=10)(arg1, arg2, foo='bar')

class BaseTask(Mutable):
    pass

################################################################################

@Reactor.extend('RQ')
class Django_RQ(Reactor.Extension):
    def initialize(self, **config):
        pass

    ############################################################################

    def enqueue(self, method, queue='default'):
        target = django_rq.get_queue(queue)

        handler = method

        if callable(handler):
            return lambda *args, **kwargs: target.enqueue(handler, *args, **kwargs)
        else:
            return None

            return lambda *args, **kwargs: None

    #***************************************************************************

    def empty(self, queue='default'):
        django_rq.get_queue(queue).empty()

    ############################################################################

    class Task(BaseTask):
        pass

    ############################################################################

    class Job(BaseTask):
        pass

################################################################################

@Reactor.extend('SCHEDULE')
class Django_Scheduler(Reactor.Extension):
    manager = property(lambda self: self._mgr)

    def initialize(self, **config):
        self._cnx = Redis()
        self._mgr = Scheduler(connection=self._cnx)

        #self.populate(self.manager)

    ############################################################################

    # Date time should be in UTC
    def run_at(self, target):
        return lambda *args, **kwargs: lambda handler: self.manager.enqueue_at(target, handler, *args, **kwargs)

    # Schedule a job to run later
    def within(self, **when):
        return lambda *args, **kwargs: lambda handler: self.manager.enqueue_in(target, handler, *args, **kwargs)

    #***************************************************************************

    def every(self, **opts):
        # Time for first execution, in UTC timezone
        if 'starts' in opts:
            opts['scheduled_time'] = opts['starts']
        if 'scheduled_time' not in opts:
            opts['scheduled_time'] = datetime.utcnow()
        # interval=60,                   # Time before the function is called again, in seconds
        # repeat=10                      # Repeat this number of times (None means repeat forever)
        # queue_name=queue_name       # In which queue the job should be put in
        return lambda *args, **kwargs: lambda handler: self.manager.schedule(func=handler,args=args, kwargs=kwargs, **opts)

    def cron(self, when, **opts):
        return lambda *args, **kwargs: lambda handler: self.manager.cron(when, func=handler,args=args, kwargs=kwargs, **opts)

    ############################################################################

    @property
    def jobs(self):
        return self.manager.get_jobs(with_times=True)

        # returns a list of tuples:
        # [(<rq.job.Job object at 0x123456789>, datetime.datetime(2012, 11, 25, 12, 30)), ...]

    #***************************************************************************

    # Reactor.schedule.since(2012, 10, 30, 10) # get all jobs until 2012-11-30 10:00:00
    def since(self, *args, **kwargs):
        return self.manager.get_jobs(until=datetime(*args, **kwargs), with_times=True)

    # Reactor.schedule.until(hours=1) # get all jobs for the next hour
    def until(self, *args, **kwargs):
        return self.manager.get_jobs(until=timedelta(*args, **kwargs), with_times=True)

