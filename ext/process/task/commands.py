from reactor.shell import *

################################################################################

@cli.command('run:sched')
#@click.option('-v', '--verbose', count=True)
@click.option('--mode', default='cli')
def scheduler(mode):
    #os.system('rqscheduler --verbose')

    if mode=='web':
        from apschedulerweb import start

        start(Reactor.schedule.manager, users={'user':'pass'})
    elif mode=='cli':
        Reactor.schedule.manager.run(burst=False)
    else:
        print "Operating mode '%s' not supportn for scheduler." % mode

#*******************************************************************************

@cli.command('run:worker')
@click.option('--queue', '-q', multiple=True)
def rq_worker(queue):
    from rq import Connection, Worker

    with Connection() as conn:
        w = Worker(queue)

        w.work()

