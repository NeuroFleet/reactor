from reactor.shortcuts import *

apath = lambda *x: rpath('..','amber',*x)

################################################################################

@cli.command('run:hub')
@click.option('--port', default=6000)
@click.option('--queue', '-q', multiple=True)
def hub(port,queue):
    from reactor.settings import WAMP_MIDDLEWARES, WAMP_PATHS
    from reactor.settings import WAMP_PROTOCOLS, WAMP_BACKENDS, WAMP_WORKERS

    cfg = {
        "type" : "router",
        "options": {
            "pythonpath": [".."]
        },
        "realms": [],
        "components": WAMP_MIDDLEWARES,
        "transports": [
            {
               "type" : "websocket",
               "endpoint" : { "type" : "tcp", "port" : 9000 }
            },{
                "type": "flashpolicy",
                "allowed_domain": "*",
                "allowed_ports": [ 8080 ],
                "endpoint": { "type": "tcp", "port": 60843 }
            },{
               "type" : "web",
               "endpoint" : { "type" : "tcp", "port" : "$PORT" },
               "paths" : WAMP_PATHS
            }
        ],
    }

    for key in ['reactor']:
        obj = {
            "name" : key,
            "roles": Reactor.fs.read_json('amber','realms',key,'roles.json'),
        }

        obj['store'] = Reactor.fs.read_json('amber','realms',key,'store.json')

        cfg['realms'].append(obj)

    cfg = {
        "version" : 2,
        "controller" : {},
        "workers" : [cfg]
    }

    for target in queue:
        cfg['workers'].append({
            "type": "guest", "executable": "python",
            "arguments": [
                "-m","reactor.shell.hub","worker",
                #"-q","default",
                "-q",target,
            ],
            "options": {
                "workdir": "../",
                "env": { "inherit": True },
                "watch": {
                    "directories": [ "../nucleon" ],
                    "action": "restart"
                }
            }
        })

    #***************************************************************************

    for proto,config in WAMP_PROTOCOLS.iteritems():
        WAMP_BACKENDS += [{
            "program":   "python",
            "arguments": ["-m","reactor.shell.bio","implant","spine","net/%s" % proto],
            #"watch":     [],
            "environ": {
                "PORT": config['PORT'],
            },
        }]

    #***************************************************************************

    for lst in [WAMP_BACKENDS]:
        for bkn in lst:
            bkn['arguments'] = bkn.get('arguments', [])

            mapping = {
                'reactor': ('python', ['-m','reactor.shell.hub']),
                'psyhub':  ('python', ['-m','reactor.shell.psy']),
                'cyborg':  ('python', ['-m','reactor.shell.bio']),
            }

            if bkn['program'] in mapping:
                runner, prefix = mapping[bkn['program']]

                bkn['program'] = runner
                bkn['arguments'] = prefix + bkn['arguments']

            entry = {
                "type": "guest",
                "executable": bkn['program'],
                "arguments": bkn['arguments'],
                "options": {
                    "workdir": bkn.get('workdir', "../"),
                    "env": {
                        "vars": {}
                    },
                }
            }

            env = bkn.get('environ', None)

            if (env is None) or (env is False):
                entry['options']['env']['inherit'] = False
            else:
                entry['options']['env']['inherit'] = True

                if type(env) is dict:
                    for k,v in env.iteritems():
                        entry['options']['env']['vars'][k] = v

            if 'watch' in bkn:
                entry['options']['watch'] = {
                    "directories": bkn['watch'],
                    "action": "restart"
                }

            cfg['workers'].append(entry)

    for entry in WAMP_WORKERS:
        cfg['workers'].append(entry)

    #***************************************************************************

    Reactor.fs.write_json(Reactor.fs.resolve('amber','config.json'), cfg)

    os.system('crossbar start --cbdir amber/')

#*******************************************************************************

@cli.command('run:web')
@click.option('--port', default=6000)
def web(port):
    os.system('django-admin runserver 0.0.0.0:%s' % port)

