from reactor.libs import *
from reactor.aliases import *

from reactor.abstract.meta import *
from reactor.abstract.core import *

#*******************************************************************************

import tastypie

################################################################################

@Reactor.extend('REST')
class Django_TastyPie(Reactor.Extension):
    def initialize(self, **config):
        self._cfg = {
            'PERSONNA': os.environ.get('PERSONNA', 'tayamino'),
            'HOSTNAME': os.environ.get('HOSTNAME', 'vernam'),
        }
        self._sess = None
        self._reg = {
            'topics':  {},
            'methods': {},
            'modules': {},
            'classes': {},
        }
        self._map = {
            'perso':       "who.%(PERSONNA)s" % self._cfg,
            'host':        "iot.%(HOSTNAME)s" % self._cfg,

            'linked':      "dat.linked",
            'record':      "dat.record",

            'media:text':  "dat.text",
            'media:video': "dat.video",

            'web:crawler': "web.crawl",
            'web:walker':  "web.walk",

            'web:walker':  "web.walk",
        }

    topics  = property(lambda self: self._reg['topics'])
    methods = property(lambda self: self._reg['methods'])

    catalog = property(lambda self: self._reg['modules'])
    classes = property(lambda self: self._reg['classes'])

    #***************************************************************************

    def resolve(self, *parts):
        path = '.'.join(parts)

        for alias,value in self._map.iteritems():
            tag = '<%s>.' % alias

            while tag in path:
                path = path.replace(tag, value)

        return path

    #***************************************************************************

    def register_topic(self, *args, **kwargs):
        def do_apply(handler, link):
            uri = self.resolve(link)

            if not hasattr(handler, '_pubsub'):
                setattr(handler, '_pubsub', [uri])
            else:
                handler._pubsub.append(uri)

            if uri not in self._reg['topics']:
                self._reg['topics'][uri] = handler

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #***************************************************************************

    def register_method(self, *args, **kwargs):
        def do_apply(handler, link):
            uri = self.resolve(link)

            if not hasattr(handler, '_revrpc'):
                setattr(handler, '_revrpc', [uri])
            else:
                handler._revrpc.append(uri)

            if uri not in self._reg['methods']:
                self._reg['methods'][uri] = handler

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    ############################################################################

    def publish(self, topic, *args, **kwargs):
        pass

    #***************************************************************************

    def call(self, method, *args, **kwargs):
        pass

