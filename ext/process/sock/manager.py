from reactor.libs import *
from reactor.aliases import *

from reactor.abstract.meta import *
from reactor.abstract.core import *

#*******************************************************************************

from twisted.internet.defer  import inlineCallbacks    as WampCallbacks

from autobahn.twisted.wamp   import ApplicationRunner  as WampAppRunner
from autobahn.twisted.wamp   import ApplicationSession as WampAppSession
from autobahn.wamp.exception import ApplicationError   as WampAppError

################################################################################

class Nucleon(WampAppSession):
    #def onConnect(self):
    #    print("Client session connected. Starting WAMP-Ticket authentication on realm '{}' as principal '{}' ..".format(self.config.realm, 'astral'))
    #
    #    self.join(self.config.realm, [u"ticket"], 'astral')

    #***************************************************************************

    #def onChallenge(self, challenge):
    #    if challenge.method == u"ticket":
    #        print("WAMP-Ticket challenge received: {}".format(challenge))
    #
    #        return 'projection'
    #    else:
    #        raise Exception("Invalid authmethod {}".format(challenge.method))

    ############################################################################

    @WampCallbacks
    def onJoin(self, details):
        catalog = dict(topics=[], methods=[])

        #print("Backend session joined: {}".format(details))

        try:
            catalog['topics'] = [kv for kv in self.topics(details)]
        except:
            for key in dir(self):
                obj = getattr(self, key)

                if callable(obj) and hasattr(obj, '_pubsub'):
                    for nrw in obj._pubsub:
                        catalog['topics'].append((nrw,obj))

        try:
            catalog['methods'] = [kv for kv in self.methods(details)]
        except:
            for key in dir(self):
                obj = getattr(self, key)

                if callable(obj) and hasattr(obj, '_revrpc'):
                    for nrw in obj._revrpc:
                        catalog['methods'].append((nrw,obj))

        for alias,handler in catalog['topics']:
            try:
                sub = yield self.subscribe(handler, alias)

                self.notify('nucleon', 'debug', "Subscribed to topic : {}".format(alias))
            except Exception as e:
                self.notify('nucleon', 'error', "Subscription failed for '{}' : {}".format(alias, e))

        for alias,handler in catalog['methods']:
            try:
                reg = yield self.register(handler, alias)

                self.notify('nucleon', 'debug', "Registered procedure : %s()" % alias)
            except Exception as e:
                self.notify('nucleon', 'error', "Could'nt register procedure '{}' : {}".format(alias, e))

        self.trigger('on_open', details)

        self.trigger('mainloop')

    ############################################################################

    def trigger(self, method, *args, **kwargs):
        hnd = getattr(self, method, None)

        if callable(hnd):
            return hnd(*args, **kwargs)
        else:
            return None

    #***************************************************************************

    def notify(self, module, level, message):
        self.publish(u'reactor.console', module, level, message)

    #***************************************************************************

    def shell(self, *stmt):
        def quote(x):
            if ' ' in x:
                return '"%s"' % x
            else:
                return x

        cmd = ' '.join([quote(x) for x in stmt])

        p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, close_fds=True)

        p.wait()

        resp = None

        if p.stdout is not None:
            resp = p.stdout.read()

        self.publish(u'reactor.process', stmt, cmd, resp)

        return resp

################################################################################

@Reactor.extend('WAMP')
class Crossbar_WAMP(Reactor.Extension):
    callbacks  = WampCallbacks
    AppSession = WampAppSession

    Nucleon = Nucleon

    def initialize(self, **config):
        self._reg = {
            'topics':  {},
            'methods': {},
        }
        self._sess = None
        self._mws = {
            'modules': {},
            'classes': {},
        }
        self._plg = {
            'modules': {},
            'classes': {},
            'keyword': {},
        }
        self._map = {
            'perso':       "who.%(PERSONNA)s" % os.environ,
            'host':        "iot.%(HOSTNAME)s" % os.environ,

            'linked':      "dat.linked",
            'record':      "dat.record",

            'media:text':  "dat.text",
            'media:video': "dat.video",

            'web:crawler': "web.crawl",
            'web:walker':  "web.walk",

            'web:walker':  "web.walk",
        }

    topics  = property(lambda self: self._reg['topics'])
    methods = property(lambda self: self._reg['methods'])

    catalog = property(lambda self: self._mws['modules'])
    plugins = property(lambda self: self._plg['modules'])

    @property
    def classes(self):
        resp = {}

        resp.update(self._mws['classes'])
        resp.update(self._cmp['classes'])

        return resp

    #*******************************************************************************

    def resolve(self, *parts):
        path = '.'.join(parts)

        for alias,value in self._map.iteritems():
            tag = '<%s>.' % alias

            while tag in path:
                path = path.replace(tag, value)

        return path

    #*******************************************************************************

    def register_topic(self, *args, **kwargs):
        def do_apply(handler, link):
            uri = self.resolve(link)

            if not hasattr(handler, '_pubsub'):
                setattr(handler, '_pubsub', [uri])
            else:
                handler._pubsub.append(uri)

            if uri not in self._reg['topics']:
                self._reg['topics'][uri] = handler

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #***************************************************************************

    def register_method(self, *args, **kwargs):
        def do_apply(handler, link, **options):
            uri = self.resolve(link)

            if not hasattr(handler, '_revrpc'):
                setattr(handler, '_revrpc', [uri])
            else:
                handler._revrpc.append(uri)

            if uri not in self._reg['methods']:
                self._reg['methods'][uri] = handler

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    ############################################################################

    def register_middleware(self, *args, **kwargs):
        def do_apply(handler, link):
            uri = self.resolve(link)

            if link not in self._mws['modules']:
                self._mws['modules'][link] = handler

            pth = '.'.join([handler.__module__,handler.__name__])

            if pth not in self._mws['classes']:
                self._mws['classes'][pth] = handler

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #***************************************************************************

    def register_component(self, *args, **kwargs):
        def do_apply(handler, link, tags=[]):
            uri = self.resolve(link)

            if link not in self._cmp['modules']:
                self._cmp['modules'][link] = handler

            pth = '.'.join([handler.__module__,handler.__name__])

            if pth not in self._cmp['classes']:
                self._cmp['classes'][pth] = handler

            for word in tags:
                if word not in self._cmp['keyword']:
                    self._cmp['keyword'][word] = []

                if handler not in self._cmp['keyword'][word]:
                    self._cmp['keyword'][word].append(handler)

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    ############################################################################

    rest_ep_p = property(lambda self: 'http://apis.uchikoma.faith:8000/publisher')
    rest_ep_c = property(lambda self: 'http://apis.uchikoma.faith:8000/caller')

    #***************************************************************************

    def publish(self, topic, *args, **kwargs):
        if self._sess is not None:
            pass
        else:
            print "Publishing '%s' : %s" % (topic,args)

            req = requests.post(self.rest_ep_p, data=json.dumps({
                'topic': topic,
                'args':  [self.dump_rest(x) for x in args]
            }))

            return self.load_rest(req.json())

    #***************************************************************************

    def call(self, method, *args, **kwargs):
        if self._sess is not None:
            pass
        else:
            print "Invoking '%s' : %s" % (method,args)

            req = requests.post(self.rest_ep_c, data=json.dumps({
                'procedure': method,
                'args':      [self.dump_rest(x) for x in args]
            }))

            return self.load_rest(req.json())

    #***************************************************************************

    def dump_rest(self, value):
        return value

    #***************************************************************************

    def load_rest(self, value):
        return value

    ############################################################################

    class Implant(Nucleon):
        pass

    ############################################################################

    class Profiler(Nucleon):
      mongo = property(lambda self: Reactor.mongo.proxy)

      #***************************************************************************

      def on_open(self, details):
        self.queue = eventlet.GreenPool(5)

        handler = getattr(self, 'on_init', None)

        if callable(handler):
            handler(details)

      #***************************************************************************

      def on_close(self, details):
        pass

      ############################################################################

      def listing(self, collection, narrow=None):
        coll = getattr(self.mongo['perso'], key, None)

        if coll!=None:
            return coll.find(narrow)
        else:
            return None

      #***************************************************************************

      def upsert(self, collection, narrow, fields):
        coll = getattr(self.mongo['perso'], key, None)

        if coll!=None:
            data = {} 

            for lst in (narrow,fields):
                data.update(lst)

            uid = coll.insert_one(data)

            return coll.find_one({
                '_id': uid,
            })
        else:
            return None

      #***************************************************************************

      def read(self, collection, narrow):
        coll = getattr(self.mongo['perso'], key, None)

        if coll!=None:
            return coll.find_one(narrow)
        else:
            return None

      #***************************************************************************

      def delete(self, collection, narrow):
        coll = getattr(self.mongo['perso'], key, None)

        if coll!=None:
            return coll.find_one_and_delete(narrow)
        else:
            return None

