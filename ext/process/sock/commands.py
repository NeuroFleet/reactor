from reactor.shell import *

from reactor.webapp.common import streams as Common_MW

Root_MW = {
    'common': Common_MW,
}

for ns in ('connector','console'):
    Root_MW[ns] = __import__('nucleon.%s.streams' % ns)

################################################################################

def resolve_middleware(path):
    resp = None

    if path.startswith('reactor.webapp.'):
        ns,path = path.replace('reactor.webapp.','').replace('streams.','').split('.', 1)
    elif path.startswith('nucleon.'):
        ns,path = path.replace('nucleon.','').replace('streams.','').split('.', 1)

    if resp is not None:
        if ns in Root_MW:
            resp = Root_MW[ns]

            for sub in path:
                if resp!=None:
                    if hasattr(resp, sub):
                        resp = getattr(resp, sub)

    #print "%s => %s" % (path,resp)

    return resp

#*******************************************************************************

def process_middleware(pool, endpoint, realm, prefix, path, suffix):
    runner = WampAppRunner(unicode(endpoint), unicode(realm))

    mod = None

    try:
        mod = resolve_middleware('reactor.streams.%s' % (prefix+path+suffix))
    except Exception,ex:
        print ex

    if mod!=None:
        try:
            runner.run(mod)
        except Exception,ex:
            pool.spawn_n(process_middleware, pool, endpoint, realm, prefix, path, suffix)

################################################################################

@cli.command('wamp:list')
#@click.argument('endpoint', default=u"ws://apis.uchikoma.faith:8000/io-sockets")
@click.option('--realm', '-r', default="reactor")
@click.option('-m', '--mode', default='all') #, choices=['all','midw','comp','subj','func'])
def nerves(realm, mode):
    def ls_midw():
        print "List of supported WAMP middlwares :"

        for alias,handler in Reactor.wamp.catalog.iteritems():
            if flag:
                print "\t-> %s" % rewrite(alias)
            else:
                print "\t=> %s" % handler

    def ls_comp():
        print "List of supported WAMP components :"

        for alias,handler in Reactor.wamp.plugins.iteritems():
            if flag:
                print "\t-> %s" % rewrite(alias)
            else:
                print "\t=> %s" % handler

    def ls_subj():
        print "List of supported WAMP methods :"

        for alias in sorted([rewrite(x) for x in Reactor.wamp.methods.keys()]):
            print "\t-> %s" % alias

    def ls_func():
        print "List of supported WAMP topics :"

        for alias in sorted([rewrite(x) for x in Reactor.wamp.topics.keys()]):
            print "\t=> %s" % alias

    flag = False

    def rewrite(alias):
        resp = realm+'.'+alias

        if not alias.startswith(realm+'.'):
            alias = realm + '.' + alias

        return alias

    mapping = dict(
        midw=[ls_midw],
        comp=[ls_comp],
        subj=[ls_subj],
        func=[ls_func],
    )

    mapping['all'] = [x for lst in mpp.values() for x in lst]

    if mode in mapping:
        for hnd in mapping[mode]:
            hnd()
    else:
        print "Unsupported view mode : %s" % mode

#*******************************************************************************

@cli.command('wamp:spinal')
@click.argument('endpoint', default=u"ws://apis.uchikoma.faith:8000/io-sockets")
@click.option('--realm', '-r', default="reactor")
@click.option('--prefix', '-p', default="")
@click.option('--middlware', '-mw', multiple=True)
@click.option('--suffix', '-s', default="")
def spinal(endpoint, realm, prefix, middlware, suffix):
    pool = eventlet.GreenPool()

    for path in middlware:
        pool.spawn_n(process_middleware, pool, endpoint, realm, prefix, path, suffix)

    pool.waitall()

################################################################################

@cli.command('wamp:implant')
@click.argument('realm')
@click.argument('narrow')
@click.option('--verse', default='default')
def implant(realm, narrow, verse):
    os.system("nodejs implant.js %s %s %s" % (realm, narrow, verse))

#*******************************************************************************

@cli.command('wamp:organ')
@click.argument('realm')
@click.option('--narrow', '-n', multiple=True)
def organs(realm, narrow):
    os.system("foreman start -f organs/%s/Procfile %s" % (realm, ' '.join(narrow)))
