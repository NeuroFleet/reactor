from reactor.shell import *

from multiprocessing import Pool,Queue,Process
from multiprocessing.managers import BaseManager

################################################################################

class BaseCompute(Mutable):
    def initialize(self, addr='0.0.0.0', port=50000, auth='abc123'):
        self.trigger('prepare')

    def manager(self):
        pass

    def worker(self):
        pass

    def pipeline(self):
        pass

#*******************************************************************************

class Compute_MPi(BaseCompute):
    def prepare(self):
        pass

    def manager(self):
        pass

    def worker(self):
        pass

    def pipeline(self):
        pass

#*******************************************************************************

class Compute_Thread(BaseCompute):
    def prepare(self):
        pass

    def manager(self):
        pass

    def worker(self):
        pass

    def pipeline(self):
        pass

#*******************************************************************************

class Compute_Dask(BaseCompute):
    def prepare(self):
        pass

    def manager(self):
        pass

    def worker(self):
        pass

    def pipeline(self):
        pass

#*******************************************************************************

class Compute_Wamp(BaseCompute):
    def prepare(self):
        pass

    def manager(self):
        pass

    def worker(self):
        pass

    def pipeline(self):
        pass

################################################################################

mapping = {
    'mpi':    Compute_MPi,
    'thread': Compute_Thread,
    'dask':   Compute_Dask,
    'wamp':   Compute_Wamp,
}

#*******************************************************************************

@cli.command('compute:host')
@click.option('--kind', default=b'thread')
@click.option('--port', default=50000)
@click.option('--auth', default=b'abc')
def server(port,auth):
    if kind in mapping:
        hnd = mapping[kind]

        obj = hnd('0.0.0.0', port, auth, 1)

        obj.manager()
    else:
        print "Unsupported computing kind : %s" % kind

#*******************************************************************************

def f(x):
    return x*x

@cli.command('compute:node')
@click.option('--kind', default=b'thread')
@click.option('--host', '-H', default='127.0.0.1')
@click.option('--port', default=50000)
@click.option('--auth', default=b'abc')
@click.option('--nums', default=4)
def client(kind,host,port,auth,nums):
    if kind in mapping:
        hnd = mapping[kind]

        obj = hnd(host, port, auth, nums)

        obj.worker()
    else:
        print "Unsupported computing kind : %s" % kind

################################################################################

@cli.command('compute:pipe')
@click.option('--kind', default=b'thread')
@click.option('--host', '-H', default='127.0.0.1')
@click.option('--port', default=6000)
@click.option('--auth', default=b'abc')
def pipeline(kind,host,port,auth):
    if kind in mapping:
        hnd = mapping[kind]

        obj = hnd(host, port, auth, nums)

        obj.pipeline()
    else:
        print "Unsupported computing kind : %s" % kind


#*******************************************************************************

@cli.command('compute:chip')
@click.option('--kind', default=b'thread')
@click.option('--host', '-H', default='127.0.0.1')
@click.option('--port', default=6000)
@click.option('--auth', default=b'abc')
def neurochip(kind,host,port,auth):
    if kind in mapping:
        hnd = mapping[kind]

        obj = hnd(host, port, auth, nums)

        obj.neurochip()
    else:
        print "Unsupported computing kind : %s" % kind

