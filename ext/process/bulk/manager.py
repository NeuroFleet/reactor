from reactor.libs import *
from reactor.aliases import *

from reactor.abstract.meta import *
from reactor.abstract.core import *

#*******************************************************************************

#import fs

################################################################################

@Reactor.extend('exe')
class Executionner(Reactor.Extension):
    def initialize(self, **config):
        pass

    #***************************************************************************

    def register_backend(self, *args, **kwargs):
        def do_apply(handler, key):
            if key not in self._reg:
                self._reg[key] = handler

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    ############################################################################

    def read(self, *path):
        return open(self.resolve(*path)).read()

    #***************************************************************************

    def write(self, path, *content):
        resp = "\n".join(content)

        with open(path, 'w+') as f:
            f.write(resp)

        return resp

    ############################################################################

    class Backend(Mutable):
        def __init__(self, prnt, nrw, **cfg):
            self._prn = prnt
            self._prx = None

            self._nrw = nrw
            self._cfg = cfg

            self.trigger('prepare')

        parent = property(lambda self: self._prn)
        proxy  = property(lambda self: self._prx)

        narrow = property(lambda self: self._nrw)
        config = property(lambda self: self._cfg)

