from reactor.libs import *
from reactor.aliases import *

from reactor.abstract.meta import *

################################################################################

class ReactorExt(Mutable):
    def __init__(self, parent, alias=None, config={}):
        self._prn = parent
        self._key = alias
        self._cfg = config

        self.trigger('initialize')

    parent = property(lambda self: self._prn)
    alias  = property(lambda self: self._key or self.__class__.__name__)
    config = property(lambda self: self._cfg)

#*******************************************************************************

class Reactor(Mutable):
    def __init__(self, exts=[]):
        for handler in exts:
                self.extend(alias=None)(handler)

        self.trigger('initialize')

    #***************************************************************************

    def extend(self, *args, **kwargs):
        def do_apply(handler, alias=None):
            if issubclass(handler, ReactorExt):
                if hasattr(handler, 'ALIAS'):
                    alias = getattr(handler, 'ALIAS')

                    delattr(handler, 'ALIAS')

                obj = handler(self, alias, {})

                setattr(self, obj.alias.lower(), obj)

                return obj
            else:
                raise Exception("Not a valid extension for the Reactor : %s" % handler)

                return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    ############################################################################

    Extension = ReactorExt

    #***************************************************************************

    class Syntax(ReactorExt):
        pass

    #***************************************************************************

    class ObjectMapper(ReactorExt):
        def initialize(self, **settings):
            self._cfg = settings.get('config', {})
            self._pki = settings.get('creds',  {})
            self._sec = settings.get('vault',  {})

            self._cnx = None

            self.trigger('prepare', **self.config)

        config = property(lambda self: self._cfg)
        creds  = property(lambda self: self._key)
        vault  = property(lambda self: self._sec)

        @property
        def proxy(self):
            if self._cnx==None:
                kw = {}

                if type(self.creds) in [dict]:
                    kw = self.creds

                self._cnx = self.trigger('connect', **kw)

            return self._cnx

################################################################################

Reactor = Reactor()

