from reactor.libs import *
from reactor.aliases import *

################################################################################

class Mutable(object):
    def trigger(self, method, *args, **kwargs):
        handler = getattr(self, method, None)

        if callable(handler):
            return handler(*args, **kwargs)
        else:
            return None

################################################################################


