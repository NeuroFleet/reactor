from __future__ import absolute_import

from celery import shared_task

import os

#from django.utils import timezone

import simplejson as json

#*******************************************************************************

ROOT_PATH = os.path.dirname(__file__)

################################################################################

@shared_task
def scan_pocket(pseudo, token):
    import pocket

    api = pocket.Pocket(settings.SOCIAL_AUTH_POCKET_KEY, token)

    query = ('state','favorite','tag','contentType','sort','detailType','search','domain','since','count','offset')

    for entry in api.get():
        entry = {"status":1,"list":{"229279689":{"item_id":"229279689",
"resolved_id":"229279689",
"given_url":"http:\/\/www.grantland.com\/blog\/the-triangle\/post\/_\/id\/38347\/ryder-cup-preview",
"given_title":"The Massive Ryder Cup Preview - The Triangle Blog - Grantland",
"favorite":"0",
"status":"0",
"resolved_title":"The Massive Ryder Cup Preview",
"resolved_url":"http:\/\/www.grantland.com\/blog\/the-triangle\/post\/_\/id\/38347\/ryder-cup-preview",
"excerpt":"The list of things I love about the Ryder Cup is so long that it could fill a (tedious) novel, and golf fans can probably guess most of them.",
"is_article":"1",
"has_video":"1",
"has_image":"1",
"word_count":"3197",
"images":{"1":{"item_id":"229279689","image_id":"1",
	"src":"http:\/\/a.espncdn.com\/combiner\/i?img=\/photo\/2012\/0927\/grant_g_ryder_cr_640.jpg&w=640&h=360",
	"width":"0","height":"0","credit":"Jamie Squire\/Getty Images","caption":""}},
"videos":{"1":{"item_id":"229279689","video_id":"1",
	"src":"http:\/\/www.youtube.com\/v\/Er34PbFkVGk?version=3&hl=en_US&rel=0",
	"width":"420","height":"315","type":"1","vid":"Er34PbFkVGk"}}}}}

#*******************************************************************************

@shared_task
def scan_sparql(alias):
    from uchikoma.satellite.vortex.models import SparqlEndpoint

    ep = SparqlEndpoint.objects.get(alias=alias)

    data = ep.execute('''SELECT ?target ?classe ?parent
WHERE {
  ?target rdf:type ?classe.
  ?target rdfs:subClassOf ?parent
}
GROUP BY ?target''')

    if type(data) in (str,unicode):
        pass
    elif type(data) in (dict,):
        if data['results']:
            print "#) Getting list of RDF ontologies on '%s' :" % ep.link

            for row in data['results']['bindings']:
                print "\t-> %(value)s ..." % row['target']

                onto,st = ep.ontologies.get_or_create(alias=row['target']['value'])

                onto.narrow = row['target']['value']
                onto.classe = row['classe']['value']
                onto.parent = row['parent']['value']

                #onto.narrow = json.dumps(row['target'])
                #onto.classe = json.dumps(row['classe'])
                #onto.parent = json.dumps(row['parent'])

                onto.save()

                #self.process(self.Wrapper(self, row['target']['value'], row))

#*******************************************************************************

@shared_task
def scan_ckan(alias):
    from uchikoma.satellite.vortex.models import CKAN_DataHub

    hub = CKAN_DataHub.objects.get(alias=alias)

    data = hub.request('GET', 'action', 'package_list')

    if data['success']:
        print "#) Getting list of CKAN datasets on '%s' :" % hub.realm

        for key in data['result']:
            data = hub.request('GET', 'action', 'package_show?id='+key)

            if data['success']:
                print "\t-> %s ..." % key

                ds,st = hub.datasets.get_or_create(alias=key)

                ds.save()

                for entry in data['result']['resources']:
                    print "\t\t*) %(name)s ..." % entry

                    res,st = ds.resources.get_or_create(id_res=entry['id'])

                    for source,target in {
                        'name':          'alias',
                        'url':           'link',
                        'description':   'summary',

                        'package_id':    'id_pkg',
                        #'id':            'id_res',
                        'revision_id':   'id_rev',

                        'format':        'type_pkg',
                        'resource_type': 'type_res',
                        'url_type':      'type_url',
                    }.iteritems():
                        if source in entry:
                            setattr(res, target, entry[source])

                    res.save()

                    #self.process(hub.Wrapper(self, res['id'], res))

################################################################################

def cortex_config(self):
    print "\t-> Configuring node : %(uid)s" % nrw

    for key in config.keys():
        app.config[key] = config[key]

    return app

#*******************************************************************************

def cortex_deploy(self):
    print "\t-> Deploying node : %(uid)s" % nrw

    if not os.path.exists('src'):
        os.system('git clone git@bitbucket.org:tayaa/cortex.git temp --recursive')

        os.chdir('temp')

    os.chdir('%s/src' % ROOT_PATH)
    os.system('git remote -v')
    os.system('git remote rm heroku')
    os.system('git remote add heroku git@heroku.com:%s.git' % (GRID_PATTERN % nrw))
    os.system('git push heroku master')

    for svc in ('web','worker'):
        if svc in app.processes:
            app.processes[svc].scale(1)

    return app

################################################################################
################################################################################
################################################################################

import os, sys, time, logging
import glob, json, yaml, requests

from slackclient import SlackClient
from pymongo import MongoClient

from django.core.management.base import BaseCommand, CommandError

from uchikoma.settings import FILES, NEUROCHIP

################################################################################

from uchikoma.vortex.models import *
from uchikoma.vortex.tasks import *

class Command(BaseCommand):
    help = 'Synchronize Linked Data sources.'

    def add_arguments(self, parser):
        pass

    db      = property(lambda self: self.coll)

    def handle(self, *args, **options):
        self.conn = MongoClient('mongodb://mist:optimum@ds039135.mlab.com:39135/vortex')
        self.coll = self.conn['vortex']

        lst = []

        print "*) Scanning SPARQL endpoints :"

        for ep in SparqlEndpoint.objects.all():
            print "\t-> %(link)s ..." % ep.__dict__

            #scan_sparql.delay(alias=ep.alias)

        print "*) Scanning CKAN hubs :"

        for hub in CKAN_DataHub.objects.all():
            print "\t-> %(realm)s ..." % hub.__dict__

            scan_ckan.delay(alias=hub.alias)

        try:
            print "*) Updating Data Hubs :"

            hubs = requests.get('http://instances.ckan.org/config/instances.json').json()
        except:
            print "#) Error while scanning for new CKAN hubs."

        for entry in hubs:
            hub,st = CKAN_DataHub.objects.get_or_create(alias=entry['id'])

            hub.realm   = entry.get('url-api', entry['url'])
            hub.version = 3

            hub.save()

            if st:
                scan_ckan.delay(alias=hub.alias)

            print "\t-> Adding CKAN Hub '%(title)s' at : %(url)s" % entry

        #try:
        #    self.bot.start()
        #except KeyboardInterrupt:
        #    sys.exit(0)
        #except:
        #    logging.exception('OOPS')

    def process(self, ds):
        ds.persist()


################################################################################
################################################################################
################################################################################

from .libs import *

def singleton(target):
    return target()

################################################################################

class DataProvider(object):
    def __init__(self):
        pass

    def wrap(self, raw, *args, **kwargs):
        return self.Wrapper(self, raw, *args, **kwargs)

#*******************************************************************************

class DataItem(object):
    def __init__(self, provider, raw, *args, **kwargs):
        self._prvd = provider
        self._item = raw

        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

    provider = property(lambda self: self._prvd)
    raw_item = property(lambda self: self._item)

#*******************************************************************************

class DataBridge(object):
    def __init__(self, provider, *args, **kwargs):
        self._prvd = provider

        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

    provider = property(lambda self: self._prvd)

################################################################################

class ApiConnector(DataBridge):
    def initialize(self, *args, **kwargs):
        self._cnx = None

        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

    @property
    def conn(self):
        if self._cnx is None:
            self._cnx = self.connect()

        return self._cnx

    def reset(self):
        self._cnx = None

#*******************************************************************************

class FileReader(DataBridge):
    def initialize(self, path):
        self._pth = path

    path = property(lambda self: self._pth)

    def read(self):
        return open(self.path).read()

################################################################################
################################################################################
################################################################################

import quepy

################################################################################

class BaseProvider(object):
    manager = property(lambda self: self._mgr)
    name    = property(lambda self: self._key)

    def __init__(self, mgr, name, *args, **kwargs):
        self._mgr = mgr
        self._key = name
        
        self._reg = {}
        
        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

    def ns(self, key, auto=False, default=None):
        if key in self._reg:
            return self._reg[key]
        elif auto:
            resp = quepy.install("neurochip.semantic.%s" % key)

            self._reg[key] = resp

            return resp
        else:
            return default
    
################################################################################

class BaseIntent(object):
    def __init__(self, prn, raw, *args, **kwargs):
        self._prn = prn
        self._raw = raw
        
        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

    provider = property(lambda self: self._prn)
    message  = property(lambda self: self._raw)

    manager  = property(lambda self: self.provider.manager)
    text     = property(lambda self: self.message['text'])

    @property
    def channel(self):
        return self.manager.slack_client.server.channels.find(self.message['channel'])

    def ns(self, *args, **kwargs): return self.provider.ns(*args, **kwargs)

################################################################################

class BaseResults(object):
    def __init__(self, prnt, raw, *args, **kwargs):
        self._won = prnt
        self._raw = raw
        
        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

    wonder  = property(lambda self: self._won)
    raw     = property(lambda self: self._raw)

    channel = property(lambda self: self.wonder.channel)

    def __getitem__(self, key,default=None):
        return self.raw.get(key, default)

################################################################################
################################################################################
################################################################################

from flask import Flask, session, redirect, url_for, escape, request, render_template

import os, sys, re

app = Flask(os.environ['SLACK_BOT'])

app.debug = True
app.instance_path = os.path.dirname(os.path.dirname(__file__))
app.secret_key    = os.environ.get('SESSION_SECRET', 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT')

################################################################################

app.jinja_options = {
    'extensions': [
        'jinja2.ext.autoescape',
        'jinja2.ext.do',
        'jinja2.ext.loopcontrols',
        'jinja2.ext.with_',
    ],
}

#*******************************************************************************

@app.context_processor
def inject_user():
    return dict(hello='world')

@app.context_processor
def utility_processor():
    def static_assets(*path):
        return '/'.join(['/static']+[x for x in path])

    def format_price(amount, currency=u'$'):
        return u'{0:.2f}{1}'.format(amount, currency)

    return dict(
        currency = format_price,
        static   = static_assets,
    )

#*******************************************************************************

class Widget(object):
    def __init__(self, template_path, title, **attrs):
        self._tpl = template_path + '.html'
        self._lbl = title
        self._hdn = None
        self._kws = attrs

    template = property(lambda self: self._tpl)

    title    = property(lambda self: self._lbl)
    heading  = property(lambda self: self._hdn or '')

    def __getitem__(self, key, default=None): return self._kws.get(key, default)

    def render(self):
        cnt = dict([(k,v) for k,v in self._kws.iteritems()])

        tpl = app.jinja_env.get_template(self.template)

        cnt['wdg']    = self
        cnt['widget'] = self

        cnt['data']   = self

        return tpl.render(cnt)

################################################################################

from celery import Celery

def make_celery(app):
    celery = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)
    TaskBase = celery.Task
    class ContextTask(TaskBase):
        abstract = True
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery.Task = ContextTask
    return celery

app.config.update(
    CELERY_BROKER_URL=os.environ.get('CLOUDAMQP_URL', 'redis://localhost:6379'),
    CELERY_RESULT_BACKEND=os.environ.get('REDISCLOUD_URL', 'redis://localhost:6379'),
)

celery = make_celery(app)

################################################################################

class View(object):
    def __init__(self, *args, **kwargs):
        if callable(getattr(self, 'initialize', None)):
            self.initialize(*args, **kwargs)

    def __call__(self, *args, **kwargs):
        if callable(getattr(self, 'invoke', None)):
            return self.invoke(*args, **kwargs)

        return None

################################################################################

class Resource(View):
    def initialize(self, *args, **kwargs):
        pass

    def invoke(self, *args, **kwargs):
        pass

################################################################################

class Adapter(View):
    def invoke(self, *args, **kwargs):
        pass

