from reactor.shortcuts import *

################################################################################

@Reactor.wamp.register_middleware('vital.Breath')
class Breath(WampAppSession):
   @WampCallbacks
   def onJoin(self, details):
        counter = 0
        pneumo  = True

        while True:
            yield self.publish('lymbic.clock.cardio', counter)

            if counter % 10:
                if pneumo:
                    yield self.publish('lymbic.pneumo.respire')
                else:
                    yield self.publish('lymbic.pneumo.expire')

                pneumo = not pneumo

            counter += 1

            yield sleep(0.8)

