from reactor.shortcuts import *

################################################################################

@Reactor.wamp.register_middleware('gateway.Authority')
class Authority(Reactor.wamp.AppSession):
   @WampCallbacks
   def onJoin(self, details):
        from reactor.settings import PRINCIPALS_DB

        def authenticate(realm, authid, details):
           ticket = details['ticket']
           
           print("WAMP-Ticket dynamic authenticator invoked: realm='{}', authid='{}', ticket='{}', peer='{}', session='{}'".format(realm, authid, ticket, details['transport']['peer'], details['session']))
           #pprint(details)

           if authid in PRINCIPALS_DB:
                if ticket == PRINCIPALS_DB[authid]['ticket']:
                   return PRINCIPALS_DB[authid]['role']
                else:
                   raise WampAppError("com.example.invalid_ticket", "could not authenticate session - invalid ticket '{}' for principal {}".format(ticket, authid))
           else:
                raise WampAppError("com.example.no_such_user", "could not authenticate session - no such principal {}".format(authid))

        try:
           yield self.register(authenticate, 'reactor.authentify')
           print("WAMP-Ticket dynamic authenticator registered!")
        except Exception as e:
           print("Failed to register dynamic authenticator: {0}".format(e))

################################################################################

@Reactor.wamp.register_middleware('gateway.Native')
class Native(Reactor.wamp.Nucleon):
    @Reactor.wamp.register_topic(u'console')
    def error_reporting(self, module, level, message):
        if level in ('debug', 'info', 'warning', 'error'):
            #print("[{}] {} [{}] {}".format(datetime.now(), module, level, message))

            print("[{}] {}".format(module, message))

        if module in ('reactor', 'hub', 'info'):
            pass # to file

    #***************************************************************************

    @Reactor.wamp.register_topic(u'process')
    def shell_commands(self, args, command, stdout):
        print("[{}] <{}> [STDOUT] {}".format(datetime.now(), 'shell', command, stdout))

    ############################################################################

    @Reactor.wamp.register_method(u'logging')
    def log_to(self, *args, **kwargs):
        return self.log(*args, **kwargs)

    #***************************************************************************

    @Reactor.wamp.register_method(u'execute')
    def run_shell(self, *args, **kwargs):
        return self.shell(*args, **kwargs)

