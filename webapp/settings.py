from reactor.utils import *

from reactor.settings import *

SECRET_KEY = 'v*kswpdyi3+*-=q4a)7&_!xwb%@udm1vi56r690!!j6e*p3^mn'

DEBUG = True

META_SITE_PROTOCOL = 'https'
META_SITE_DOMAIN   = MAIN_DOMAIN

ALLOWED_HOSTS = [
    'localhost', MAIN_DOMAIN,
]

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    #'subdomains',
    #'django_jinja',
    'revproxy',
    'django_rq',
    'social_django',
    'meta',
] + [
    'nucleon.%s' % app
    for app in APPLETs.keys()
]

from .backends import *

MIDDLEWARE = [
    #'subdomains.middleware.SubdomainURLRoutingMiddleware',
    'reactor.webapp.middlewares.vHosting',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'reactor.webapp.views.generic'

SUBDOMAIN_URLCONFS = {
    None:   'reactor.webapp.views.landing',
    'jobs': 'reactor.webapp.views.jobs',
}

for app,cfg in APPLETs.iteritems():
    for fqdn,path in cfg.get('urlconf', {}).iteritems():
        SUBDOMAIN_URLCONFS[fqdn] = 'nucleon.%s.views.%s' % (app,path)

HOST_MIDDLEWARE_URLCONF_MAP = {
    #'jobs.%s' % MAIN_DOMAIN: '',
}

for dns,url in SUBDOMAIN_URLCONFS.iteritems():
    if dns is None:
        HOST_MIDDLEWARE_URLCONF_MAP[MAIN_DOMAIN] = url

        dns = 'www'

    HOST_MIDDLEWARE_URLCONF_MAP['%s.%s' % (dns,MAIN_DOMAIN)] = url

#TEMPLATES = [ JINJA_TEMPLATE, DJANGO_TEMPLATE ]

TEMPLATES = [ DJANGO_TEMPLATE ]

WSGI_APPLICATION = 'reactor.webapp.wsgi.application'

################################################################################

AUTHENTICATION_BACKENDS = Reactor.social.get_backends(globals(),
    'social_core.backends.email.EmailAuth',
    'social_core.backends.username.UsernameAuth',
    'django.contrib.auth.backends.ModelBackend',
)


LOGIN_URL = '/auth/login/'
LOGIN_REDIRECT_URL = '/auth/profile/'
SOCIAL_AUTH_STRATEGY = 'social_django.strategy.DjangoStrategy'
SOCIAL_AUTH_STORAGE = 'social_django.models.DjangoStorage'
# SOCIAL_AUTH_STORAGE = 'app.models.CustomDjangoStorage'
SOCIAL_AUTH_GOOGLE_OAUTH_SCOPE = [
    'https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/userinfo.profile'
]
SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE = [
    'https://www.googleapis.com/auth/plus.login'
]
SOCIAL_AUTH_EMAIL_FORM_HTML = 'email_signup.html'
SOCIAL_AUTH_EMAIL_VALIDATION_FUNCTION = 'app.mail.send_validation'
SOCIAL_AUTH_EMAIL_VALIDATION_URL = '/email-sent/'
SOCIAL_AUTH_USERNAME_FORM_HTML = 'username_signup.html'

SOCIAL_AUTH_PIPELINE = (
    'social_core.pipeline.social_auth.social_details',
    'social_core.pipeline.social_auth.social_uid',
    'social_core.pipeline.social_auth.auth_allowed',
    'social_core.pipeline.social_auth.social_user',
    'social_core.pipeline.user.get_username',
    'reactor.webapp.common.pipeline.require_email',
    'social_core.pipeline.mail.mail_validation',
    'social_core.pipeline.user.create_user',
    'social_core.pipeline.social_auth.associate_user',
    'social_core.pipeline.debug.debug',
    'social_core.pipeline.social_auth.load_extra_data',
    'social_core.pipeline.user.user_details',
    'social_core.pipeline.debug.debug'
)

#RQ_EXCEPTION_HANDLERS = ['path.to.my.handler']

CACHES['default']            = CACHES.get('grid', CACHES['local'])
RQ_QUEUES['default']         = RQ_QUEUES.get('grid', RQ_QUEUES['local'])

DATABASES['default']         = DATABASES.get('grid', DATABASES['local'])
MONGODB_DATABASES['default'] = MONGODB_DATABASES.get('grid', MONGODB_DATABASES['local'])
NEO4J_DATABASES['default']   = NEO4J_DATABASES.get('grid', NEO4J_DATABASES['local'])

# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/

MEDIA_URL  = '/media/'
MEDIA_ROOT = rpath('media')

STATICFILES_DIRS = [
    rpath('assets'),
]

STATIC_URL  = '/static/'
STATIC_ROOT = rpath('static')

MIDDLEWARE_CLASSES = MIDDLEWARE

try:
    from reactor.webapp.local_settings import *
except ImportError:
    pass

