PROTOCOLs = (
    ('postgres', "PostgreSQL database"),
    ('mysql',    "MySQL database"),

    ('mongodb',  "MongoDB server"),
    ('couchdb',  "CouchDB server"),

    ('gremlin',  "Gremlin"),
    ('neo4j',    "Neo4j Cypher"),

    ('hdfs',     "Hadoop F.S"),

    ('elastic',  "Elastic Search"),
)

FORMATs = (
    ('json',     "JSON"),
    ('xml',      "XML"),
    ('yaml',     "YAML"),

    ('json-ld',  "JSON-LD"),
    ('rdf',      "RDF"),
    ('n3',       "N3 / Turtle"),

    ('geojson',  "GeoJSON"),
    ('xml',      "XML"),
    ('yaml',     "YAML"),
)

CORTEXs = (
    ('wernick', "Wernick"),
    ('broca',   "Broca"),

    ('reason',  "Reasoner"),
    ('learn',   "Learner"),
    ('assess',  "Assessement"),
    ('witness', "Witnessing"),
)

SPINEs = (
    ('acoustic', "Acoustic Nerve"),
    ('visual',   "Visual Nerve"),
)

NEURO_ANATOMY = [
    ('face',       "Body :: Face"),

    ('broca',      "Linguist :: Broca"),
    ('wernick',    "Linguist :: Wernick"),

    ('dialect',    "Linguist :: Dialect"),
    ('glossary',   "Linguist :: Glossary"),

    ('rdf-graph',  "RDF :: Graph"),
    ('rdf-mapper', "RDF :: Mapper"),
    ('rdf-reason', "RDF :: Reasoner"),

    ('crawler',    "Misc :: Crawler"),
    ('spider',     "Misc :: Spider"),
    ('walker',     "Misc :: Walker"),
]

NEURO_HUBs = (
    ('sparql', "SPARQL endpoint"),

    ('hudv',   "Harvard University DataVerse"),
    ('okfn',   "Open Knowledge Foundation"),

    ('soda',   "Socrata LinkedData"),
    ('ckan',   "Comprehensive Knowledge Archive Network"),
)

