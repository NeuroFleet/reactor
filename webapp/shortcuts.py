from reactor.helpers import *

import json

from django.core.cache import cache

from django.conf import settings
from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout as auth_logout, login
from django.contrib.auth.models import User

from social_core.backends.oauth import BaseOAuth1, BaseOAuth2
from social_core.backends.google import GooglePlusAuth
from social_core.backends.utils import load_backends
from social_django.utils import psa, load_strategy

from functools import wraps

from django.conf import settings
from django.shortcuts import render

from reactor.webapp.common.utils import common_context

from social_django.utils import load_strategy

from django import forms
from django.db import models

from django.contrib.auth.models import AbstractBaseUser
from social_django.models import AbstractUserSocialAuth, DjangoStorage, USER_MODEL

from django.template import TemplateDoesNotExist, RequestContext, loader as template_loader

def render_to(template):
    """Simple render_to decorator"""
    def decorator(func):
        """Decorator"""
        @wraps(func)
        def wrapper(request, *args, **kwargs):
            """Rendering method"""
            out = func(request, *args, **kwargs) or {}
            if isinstance(out, dict):
                out = render(request, template, common_context(
                    settings.AUTHENTICATION_BACKENDS,
                    load_strategy(),
                    request.user,
                    plus_id=getattr(settings, 'SOCIAL_AUTH_GOOGLE_PLUS_KEY', None),
                    **out
                ))
            return out
        return wrapper
    return decorator

import django_rq

import reactor.contrib.apis

from reactor.contrib.shortcuts import *
from reactor.ext.shortcuts import *

from twisted.internet.defer  import inlineCallbacks    as WampCallbacks
