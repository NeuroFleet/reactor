#-*- encoding: utf-8 -*-

from reactor.utils import *

from reactor.settings import *

################################################################################

PROTO = dict(
    redis={
        'local': dict(host='localhost', port=6379, base=0),
        'queue': dict(host='localhost', port=6379, base=1),
    },
    mongo={
        'local':    dict(host='localhost', port=27017, name='reactor'),
        'shivhack': dict(
            host='ds039165.mlab.com', port=39165, name='shivhack',
            user='mist', pswd='1932b6d2-1bc7-441a-8a71-a803bb2fddca',
        ),
        'vortex':   dict(
            host='ds039135.mlab.com', port=39135, name='vortex',
            user='mist', pswd='1932b6d2-1bc7-441a-8a71-a803bb2fddca',
        ),
    },
    neo4j={
        'local': dict(host='localhost', port=7474),
        'scuba': dict(
            host='hobby-bnenchpclkjdgbkemiemhgml.dbs.graphenedb.com', port=24789,
            user='scuba_dev', pswd='zdZ4ejSxxgMMtRdSwhcT',
        ),
    },
)

PROTO['mongo']['default'] = PROTO['mongo']['shivhack']

PROTO['neo4j']['default'] = PROTO['neo4j']['scuba']

################################################################################

CACHES = {
    "local": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://%(host)s:%(port)d/%(base)d" % PROTO['redis']['local'],
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    },
}

#*******************************************************************************

DATABASES = {
    'local': {
        'ENGINE': 'django.db.backends.sqlite3', 'NAME': rpath('main.sqlite'),
    },
}

#*******************************************************************************

MONGODB_DATABASES = {
    'local':    { 'LINK': "mongodb://%(host)s:%(port)d/%(name)s" % PROTO['mongo']['local'] },
    'shivhack': { 'LINK': "mongodb://mist:1932b6d2-1bc7-441a-8a71-a803bb2fddca@ds039165.mlab.com:39165/shivhack" },
    'vortex':   { 'LINK': "mongodb://mist:1932b6d2-1bc7-441a-8a71-a803bb2fddca@ds039135.mlab.com:39135/vortex" },
}

#*******************************************************************************

NEO4J_DATABASES = {
    'local': { 'HOST': "localhost", 'PORT': "7474" },
    'scubadev': {
        'HOST': "hobby-bnenchpclkjdgbkemiemhgml.dbs.graphenedb.com", 'PORT': "24789",
        'USERNAME': "scuba_dev", 'PASSWORD': "zdZ4ejSxxgMMtRdSwhcT",
    },
}

################################################################################

RQ_QUEUES = {
    'local': {
        'HOST': 'localhost',
        'PORT': 6379,
        'DB': 1,
        #'PASSWORD': 'some-password',
        'DEFAULT_TIMEOUT': 360,
    },
}

for src,lst in [
    ('local', ['embedded','social','crawler']),
]:
    for dest in lst:
        RQ_QUEUES[dest] = RQ_QUEUES[src]

#*******************************************************************************

MQTT_QUEUES = {
    'local': {
        'HOST': 'localhost',
        'PORT': 1883,
    },
}

################################################################################

JINJA_TEMPLATE = {
    'BACKEND': 'django_jinja.backend.Jinja2',
    'DIRS': [
        rpath('views'),
        rpath('templates'),
    ],
    'APP_DIRS': True,
    'OPTIONS': {
        'match_extension': '.html',
        #'match_regex': r'^!(admin/).*',
        'filters': {
            'backend_name':    'nucleon.connector.templatetags.reactor.backend_name',
            'backend_class':   'nucleon.connector.templatetags.reactor.backend_class',
            'icon_name':       'nucleon.connector.templatetags.reactor.icon_name',
            'social_backends': 'nucleon.connector.templatetags.reactor.social_backends',
            'legacy_backends': 'nucleon.connector.templatetags.reactor.legacy_backends',
            'oauth_backends':  'nucleon.connector.templatetags.reactor.oauth_backends',
            'filter_backends': 'nucleon.connector.templatetags.reactor.filter_backends',
            'slice_by':        'nucleon.connector.templatetags.reactor.slice_by',
                'human_number':    'nucleon.connector.templatetags.reactor.human_number',
                'human_size':      'nucleon.connector.templatetags.reactor.human_size',
            'fa_icon':         'nucleon.connector.templatetags.reactor.icon_name',
                'since':      'nucleon.connector.templatetags.reactor.since',
                'price':      'nucleon.connector.templatetags.reactor.price',
                'percent':    'nucleon.connector.templatetags.reactor.percent',
            'reverse':    'nucleon.connector.templatetags.reactor.reverse',
                'form_field': 'nucleon.connector.templatetags.reactor.form_field',
                'widget': 'nucleon.connector.templatetags.reactor.widget',
                'json':   'nucleon.connector.templatetags.reactor.json',
        },
        'context_processors': [
            'django.template.context_processors.debug',
            'django.template.context_processors.request',
            'django.contrib.auth.context_processors.auth',
            'django.contrib.messages.context_processors.messages',
            'social_django.context_processors.backends',
            'reactor.webapp.middlewares.Enrich',
        ],
    }
}

#*******************************************************************************

DJANGO_TEMPLATE = {
    'BACKEND': 'django.template.backends.django.DjangoTemplates',
    'DIRS': [
        rpath('views'),
        rpath('templates'),
    ],
    'APP_DIRS': True,
    'OPTIONS': {
        'context_processors': [
            'django.template.context_processors.debug',
            'django.template.context_processors.request',
            'django.contrib.auth.context_processors.auth',
            'django.contrib.messages.context_processors.messages',
            'social_django.context_processors.backends',
            'reactor.webapp.middlewares.Enrich',
        ],
    },
}

################################################################################

if GRID_DNS is not None:
    CACHES['grid'] = {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://redis.cache.%s:6379/7" % GRID_DNS,
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }

    RQ_QUEUES['grid'] = {
        'HOST': 'redis.cache.%s' % GRID_DNS,
        'PORT': 6379,
        'DB': 8,
        'DEFAULT_TIMEOUT': 360,
    }

    #***************************************************************************

    DATABASES['grid'] = {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME':   rpath('main.sqlite'),
    }

    MONGODB_DATABASES['grid'] = {
        'LINK': "mongodb://mongo.nosql.%s:27017/reactor" % GRID_DNS,
    }

    NEO4J_DATABASES['grid'] = {
        'HOST': "graph.nosql.%s" % GRID_DNS,
        'PORT': "7474",
    }

