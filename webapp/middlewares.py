#-*- coding: utf-8 -*-

from reactor.utils import *

import time
from django.conf import settings
from django.utils.cache import patch_vary_headers

################################################################################

class vHosting:
    def process_request(self, request):
        try:
            request.META["LoadingStart"] = time.time()
            host = request.META["HTTP_HOST"]
            #if host[-3:] == ":80":
            #    host = host[:-3] # ignore default port number, if present

            # best way to do this.
            host_port = host.split(':')
            if len(host_port)==2:                    
                host = host_port[0] 

            if host in settings.HOST_MIDDLEWARE_URLCONF_MAP:
                request.urlconf = settings.HOST_MIDDLEWARE_URLCONF_MAP[host]
                request.META["MultiHost"] = str(request.urlconf)
            else:
                request.META["MultiHost"] = str(settings.ROOT_URLCONF)

        except KeyError:
            pass # use default urlconf (settings.ROOT_URLCONF)

    def process_response(self, request, response):
        if 'MultiHost' in request.META:
            response['MultiHost'] = request.META.get("MultiHost")

        if 'LoadingStart' in request.META:
            _loading_time = time.time() - int(request.META["LoadingStart"])
            response['LoadingTime'] = "%.2fs" % ( _loading_time, )

        if getattr(request, "urlconf", None):
            patch_vary_headers(response, ('Host',))
        return response

################################################################################

def Enrich(request):
    resp = dict(
        wamp=dict(
            hostname   = request.META.get('HTTP_HOST', 'apis.uchikoma.faith:8000'),
            auth_id    = 'webui',
            auth_token = '123sekret',
        ),
        empty_list=[],
        empty_dict={},
        empty_graph={
            'nodes': [],
            'edges': [],
        },
        page = {
            'title':     "xHub",
            'section':   'console',
            'urlconf':   'reactor.webapp.wsgi',
            'domain':    'uchikoma.faith',
            'subdomain': 'www',
        },
        catalog = {
            'subdomain': {},
            #'grid': dict([
            #    (x, [pth.replace('.html','') for pth in os.listdir(rpath(x)) if pth.endswith('.html')])
            #    for x in ('skin','layouts')
            #]+[
            #    (x+'s', [pth.replace('.html','') for pth in os.listdir(rpath('templates','blocks',x)) if pth.endswith('.html')])
            #    for x in ('panel','viewer','widget')
            #]),
            'pages': dict([
                (x, [pth.replace('.html','') for pth in os.listdir(rpath('views',y)) if pth.endswith('.html')])
                for x,y in [
                    ('auth',     'connector'),
                    ('console',  'console'),
                    ('devops',   'devops'),
                    ('explorer', 'explor'),
                    ('hobbit',   'hobbit'),
                    ('infosec',  'infosec'),
                    ('linguist', 'linguist'),
                    ('linked',   'linked'),
                ]
            ]),
            #'ontology': {
            #    'search': [pth.replace('.html','') for pth in os.listdir(rpath('templates','search')) if pth.endswith('.html')],
            #},
            #'custom': {
            #    'special': [pth.replace('.html','') for pth in os.listdir(rpath('templates','special')) if pth.endswith('.html')],
            #    'status':  [pth.replace('.html','') for pth in os.listdir(rpath('templates','status')) if pth.endswith('.html')],
            #    'remote':  [pth.replace('.html','') for pth in os.listdir(rpath('templates','special','remote')) if pth.endswith('.html')],
            #    'screen':  [pth.replace('.html','') for pth in os.listdir(rpath('templates','special','screen')) if pth.endswith('.html')],
            #},
        },
    )

    if request.user.is_authenticated():
        resp['identity'] = {
            'pseudo':     request.user.username,
            'full_name':  '%s %s' % (request.user.last_name, request.user.first_name),
            'full_title': 'TAYAA Med Amine',
            'email':      request.user.email,
            'mugshot':    'mugshot/tayamino.jpg',
            'notifications': [
            ],
        }
    else:
        resp['identity'] = {
            'pseudo':     "anonymous",
            'full_name':  'John Doe',
            'full_title': 'John Doe',
            'email':      'john@doe.me',
            'mugshot':    'img/user-13.jpg', # 'mugshot/tayamino.jpg',
            'notifications': [
            ],
        }

    #resp['page']['menus'] = [
    #    resp['catalog']['pages'],
    #    resp['catalog']['custom'],
    #    resp['catalog']['grid'],
    #]

    return resp

