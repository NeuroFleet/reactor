from django.conf.urls import url, include

from revproxy.views import ProxyView

################################################################################

class SupervisorProxy(ProxyView):
    upstream = 'http://example.com'

    #rewrite = (
    #    (r'^/yellow/star/$', r'/black/hole/'),
    #    (r'^/red/?$', r'http://www.mozilla.org/'),
    #
    #    (r'^/foo/(.*)$', r'/bar\1'),
    #)

################################################################################

urlpatterns = [
    url(r'^(?P<path>.*)$', SupervisorProxy.as_view()),
]
