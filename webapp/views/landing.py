from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin

from nucleon.connector.views import landing as LandingPage

urlpatterns = [
    url(r'^admin/',    admin.site.urls),
    url(r'^queues/',   include('django_rq.urls')),

    url(r'^auth/',     include('nucleon.connector.views')),
    url(r'^social/',   include('social_django.urls')),
] + [
    url('^%s/' % sub, include('nucleon.%s.views' % app))
    for sub,app in [
        ('auth',     'connector'),
        ('console',  'console'),
        ('devops',   'devops'),
        ('explorer', 'explor'),
        ('hobbit',   'hobbit'),
        ('infosec',  'infosec'),
        ('linguist', 'linguist'),
        ('linked',   'linked'),
    ]
] + [
    url(r'^$',         LandingPage),
]

urlpatterns += static(settings.MEDIA_URL,  document_root=settings.MEDIA_ROOT)

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

