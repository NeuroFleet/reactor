from reactor.shortcuts import *

################################################################################

#@cli.command()
#@click.option('--port', default=6000)
def web(port):
    pass

#*******************************************************************************

#@cli.command()
#@click.option('--queue', '-q', multiple=True)
def worker(queue):
    pass

#*******************************************************************************

#@cli.command()
#@click.option('-v', '--verbose', count=True)
def scheduler(port):
    pass

################################################################################

if __name__ == '__main__':
    #import reactor.workers

    cli(default_map={
        'web': {
            'port': os.environ.get('PORT', 8000),
        }
    })

